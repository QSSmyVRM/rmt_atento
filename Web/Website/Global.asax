﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>

<%@ Application Language="C#" %>

<script runat="server">
    
    ns_Logger.Logger log; // ZD 100170
    
    
    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup
        Application["OnlineUserList"] = "";
        //ReadCOMConfig();
        Application.Lock();
        //Version and Year upgradation start 
        Application.Remove("CopyrightsDur");
        Application.Remove("Version");
        Application.Add("Version", "2.9116.3.1");//Can Edit for version upgradation //FB 2864
        Application.Add("CopyrightsDur", "2002-2016");//FB 1814//Can Edit for year upgradation //FB 2682
        //Version and Year upgradation end
        Application.UnLock();

    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs     
        // ZD 100170  
        string language = "en", URL = "../en/showerror.aspx";
        if (HttpContext.Current.Session["language"] != null)
            language = HttpContext.Current.Session["language"].ToString();

        URL = "../" + language + "/showerror.aspx";
        
        string exPage = HttpContext.Current.Request.Url.LocalPath.ToLower();
        Exception ex = Server.GetLastError();
        Server.ClearError();
        log = new ns_Logger.Logger();
        log.Trace("System Error" + ex.StackTrace + " : " + ex.Message);

        Response.Redirect(URL);
    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started
        Session["DefaultDateType"] = "MM/dd/yyyy";
        ReadCOMConfig();

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
        if(Session["userID"] != null )
        {
            if(Session["userID"].ToString().Trim() != "")
            {
                if (Application["OnlineUserList"] != null)
                {
                    string localUserList = Application["OnlineUserList"].ToString();
                    string userid = Session["userID"].ToString().Trim();
                    string[] usrArray = localUserList.Split(',');

                    if (usrArray.Length > 0)
                    {
                        localUserList = "";
                        for (int lp = 0; lp < usrArray.Length; lp++)
                        {
                            if (usrArray[lp].ToString().Trim() == userid)
                                continue;

                            localUserList = localUserList + usrArray[lp].ToString().Trim() + ",";
                        }
                        Application.Lock();
                        Application["OnlineUserList"] = localUserList;
                        Application.UnLock();
                    }
                }
            }
        }
    }
    //ZD 100263 START
    protected void Application_PreSendRequestHeaders()
    {
        HttpContext.Current.Response.Headers.Remove("Server");
        HttpContext.Current.Response.Headers.Remove("X-AspNet-Version");
        HttpContext.Current.Response.Headers.Remove("Cache-Control");
        HttpContext.Current.Response.Headers.Remove("Date");
        HttpContext.Current.Response.Headers.Remove("Content-Length");
        HttpContext.Current.Response.Headers.Remove("ETag");
        HttpContext.Current.Response.Headers.Remove("Last-Modified");
        HttpContext.Current.Response.Headers.Remove("Connection");
        HttpContext.Current.Response.Headers.Remove("Accept-Ranges");
        HttpContext.Current.Response.Headers.Remove("Cookie");
        HttpContext.Current.Response.Headers.Remove("Vary");
       
    }
    //ZD 100263 END
    /// <summary>
    /// ReadCOMConfig
    /// To retreive XML value from the vrmwebconfig.xml
	/// and put to the Application variables for use throughout the session
    /// </summary>
    private void ReadCOMConfig()
    {
        //msxml4_Net.DOMDocument40Class XmlDoc = null; //Modified to remove msXML refs
        System.Xml.XmlDocument XmlDoc = null;
        try
        {
            XmlDoc = new System.Xml.XmlDocument(); // msxml4_Net.DOMDocument40Class();
           // XmlDoc.async = false;
            XmlDoc.Load("C:\\VRMSchemas_v1.8.3\\VRMConfig.xml");

            Application["mode"] = XmlDoc.SelectSingleNode("//VRMConfig/WebMode").InnerText.Trim();
            Application["debugMode"] = XmlDoc.SelectSingleNode("//VRMConfig/DebugMode").InnerText.Trim();
            //ZD 101525
            //Application["ssoMode"] = XmlDoc.SelectSingleNode("//VRMConfig/SSOMode").InnerText.Trim().ToLower();
            Application["Client"] = XmlDoc.SelectSingleNode("//VRMConfig/Client").InnerText.Trim().ToLower();
            //Application["confRecurrence"] = XmlDoc.SelectSingleNode("//VRMConfig/Conf_Reccurance").InnerText.Trim().ToLower();
            Application["MaxConferenceDurationInHours"] = XmlDoc.SelectSingleNode("//VRMConfig/MaxConferenceDurationInHours").InnerText.Trim();

            Application["CosignEnable"] = XmlDoc.SelectSingleNode("//VRMConfig/CosignEnable").InnerText.Trim();
            Application["LDAPDomain"] = XmlDoc.SelectSingleNode("//VRMConfig/LDAPDomain").InnerText.Trim();
            Application["External"] = XmlDoc.SelectSingleNode("//VRMConfig/External").InnerText.Trim(); //FB 2363
            Application["Exchange"] = XmlDoc.SelectSingleNode("//VRMConfig/Exchange").InnerText.Trim(); //FB 2457 Exchange Round Trip
            if(XmlDoc.SelectSingleNode("//VRMConfig/ConfImport") != null)
                Application["ConfImport"] = XmlDoc.SelectSingleNode("//VRMConfig/ConfImport").InnerText.Trim();//ZD 102029
            
			
            //if(Application["confRecurrence"] == null)
            //{
            //    Application["confRecurrence"] = 30;
            //}
            //if(Application["confRecurrence"].ToString() == "")
            //{
            //    Application["confRecurrence"] = 30;
            //}
	        if(Application["MaxConferenceDurationInHours"] == null)
            {
		        Application["MaxConferenceDurationInHours"] = 24;
            }
            if(Application["MaxConferenceDurationInHours"].ToString() == "")
            {
                Application["MaxConferenceDurationInHours"] = 24;
            }
	        XmlDoc = null;
        }
        catch (Exception) { }
    }
           
</script>

