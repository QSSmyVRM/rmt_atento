<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" CodeFile="ldapimport.aspx.cs" AutoEventWireup="true"  Inherits="test.LDAPImport" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<script type="text/javascript">
// <![CDATA[
function pageBarFirstButton_Click(){
    grd1.GotoPage(0);
}
function pageBarPrevButton_Click(){
    grd1.PrevPage();
}
function pageBarNextButton_Click(){
    grd1.NextPage();
}
function pageBarLastButton_Click(s, e){
    grd1.GotoPage(grd1.cpPageCount - 1);
}
function pageBarTextBox_Init(s, e) {
    s.SetText(s.cpText);
}
function pageBarTextBox_KeyPress(s, e){
    if(e.htmlEvent.keyCode != 13)
        return;
    e.htmlEvent.cancelBubble = true;
    e.htmlEvent.returnValue = false;
    var pageIndex = (parseInt(s.GetText()) <= 0) ? 0 : parseInt(s.GetText()) - 1;
    grd1.GotoPage(pageIndex);
}
function pageBarTextBox_ValueChanged(s, e){
    var pageIndex = (parseInt(s.GetText()) <= 0) ? 0 : parseInt(s.GetText()) - 1;
    grd1.GotoPage(pageIndex);
}
function pagerBarComboBox_SelectedIndexChanged(){
    grd1.PerformCallback(pagerBarComboBox_SelectedIndexChanged.arguments[0].value);
}
//ZD 100604 start
var img = new Image();
img.src = "../en/image/wait1.gif";
//ZD 100604 End
function DisableCombo()
{
    if(document.getElementById('chkSelectAll').checked)
    {
        //document.getElementById('comboBox').enabled = false;
        document.getElementById('grd1$PagerBarT$ctl06').disabled=true;
    }
}
//FB 2320 - Starts
function setonScroll(divObj,DgID)  
{
    var datagrid = document.getElementById("LdapListTbl");
    var HeaderCells = datagrid.getElementsByTagName('th');
    var HeaderRow;
    if(HeaderCells == null || HeaderCells.length == 0)
    {
        var AllRows = datagrid.getElementsByTagName('tr');
        HeaderRow = AllRows[0];        
    }
    else
    {
        HeaderRow = HeaderCells[0].parentNode;        
    }            
    
    var DivsTopPosition = parseInt(divObj.scrollTop);
    
    if(DivsTopPosition>0)
    {
        HeaderRow.style.position = 'absolute';
        HeaderRow.style.top = (parseInt(DivsTopPosition)).toString() + 'px';
        HeaderRow.style.width = datagrid.style.width;                
        HeaderRow.style.zIndex='1000';
    }
    else
    {
        divObj.scrollTop = 0;
        HeaderRow.style.position = 'relative';
        HeaderRow.style.top = '0';
        HeaderRow.style.bottom='0';
        HeaderRow.style.zIndex='0';                
    }
}
//FB 2320 - End
</script>




<%--ZD 100540 Starts--%>
<script type="text/javascript">
    function fnfocusselected() {

        if (navigator.appName.indexOf('Internet Explorer') != -1) {
            var tagname = document.querySelectorAll('.name')[1].id;
            document.getElementById(tagname).focus();

            document.getElementById(tagname).className = "";

            setTimeout("document.getElementById('" + tagname + "').parentNode.previousSibling.childNodes[0].click();", 500);
        }
        else {
            var tagname = document.getElementsByClassName('name')[1].id;
            document.getElementById(tagname).focus();
            document.getElementById(tagname).className = "";
            setTimeout("document.getElementById('" + tagname + "').parentNode.previousElementSibling.childNodes[0].click();", 100);
        }

    }

</script>
<%--ZD 100540 Ends--%>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>LDAP Import</title>
    <link rel="stylesheet" type="text/css" media="all" href="css/aqua/theme.css" title="Aqua" />
    
</head>
<body>
    <form id="frmLDAPBrowser" runat="server">
       <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
		<%--PSU START--%>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <table width="95%">
                        <tr>
                            <td align="center">
                                 <center><h3><asp:Literal Text="<%$ Resources:WebResources, LDAPImport_LDAPDirectory%>" runat="server"></asp:Literal></h3></center><!-- FB 2570 -->
                                 <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                                    <img border='0' src='image/wait1.gif' alt='Loading..' />
                                </div><%--ZD 100678--%>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server"  UpdateMode="Conditional" RenderMode="Inline" >
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="BtnUpdateRHS"  />
                                    </Triggers>
                                    <ContentTemplate>
                                            <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" CssClass="lblError"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
            <%--ZD 100540 starts--%><%-- alignment fix--%>
                <td align="left">
                  <table width="100%">
                    <tr class="tableBody">
                        <td style="width:20%"> 
                            <table cellpadding="3" cellspacing="0" border="0" width="100%" style="border-color:Blue;border-width:1px;border-style:Solid;">
                                <tr>
                                    <td class="tableHeader" align="center"><asp:Literal Text="<%$ Resources:WebResources, LDAPImport_ServerAddress%>" runat="server"></asp:Literal></td>
                                    <td class="tableHeader" align="center"><asp:Literal Text="<%$ Resources:WebResources, LDAPImport_Domain%>" runat="server"></asp:Literal></td>
                                    <td class="tableHeader" align="center"><asp:Literal Text="<%$ Resources:WebResources, LDAPImport_Login%>" runat="server"></asp:Literal></td>
                                </tr>
                                 <tr class="tableBody">
                                    <td align="center"><asp:Label ID="lblServeraddr" runat="server" /><asp:Label ID="lblSrchFilter" runat="server" Style="display: none;" /></td>
                                    <td align="center"><asp:Label ID="lblDomain" runat="server" /></td>
                                    <td align="center">
                                        <asp:Label ID="lblLoginname" runat="server" /> 
                                        <asp:Label ID="lblPassword" runat="server" Visible="false"/>
                                    </td>
                                </tr>
                             </table>   
                        </td>
                        <td width="60%" style="border-color:Blue;border-width:1px;border-style:Solid;">
                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                <tr >
                                    <td align="center" class="blackblodtext" width="70%">
                                        <asp:Literal Text="<%$ Resources:WebResources, LDAPImport_SearchFilter%>" runat="server"></asp:Literal> <asp:TextBox ID="txtSearchUsers" style="vertical-align: middle;" runat="server" width="290px" Text="" CssClass="altText"/><%--ZD 100429--%>
                                        
                                        </td><td><button ID="btnDfltSrch" runat="server" style="vertical-align: middle;" onclick="DataLoading(1);" class="altMedium0BlueButtonFormat"  onserverclick="btnDfltSrch_Click">
											<asp:Literal Text="<%$ Resources:WebResources, LDAPImport_btnDfltSrch%>" runat="server"></asp:Literal></button> <%--ZD 100369--%>
                                        </td><td><button ID="btnSrch1" runat="server" style="vertical-align: middle;width:168px" onclick="DataLoading(1);" class="altMedium0BlueButtonFormat" onserverclick="btnSrch1_Click">
                                             <asp:Literal Text="<%$ Resources:WebResources, LDAPImport_btnSrch1%>" runat="server"></asp:Literal></button> <%--ZD 100369--%>
                                    </td>
                               </tr> 
                            </table>                       
                        </td> 
                    </tr>
                    <%--ZD 100540 Ends--%>
                </table>  
                </td>
            </tr>
            <tr>
                <td>
                <table width="100%"><tr><td align="left" width="100%">
                      <asp:UpdatePanel ID="UpdatePanel2" runat="server"  UpdateMode="Conditional" RenderMode="Inline">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="BtnUpdateRHS"  />
                        </Triggers>
                        <ContentTemplate>
                            <table width="100%" cellpadding="0" cellspacing="0"> 
                                <tr>
                                    <td  valign="top">
                                        <table  width="99%" cellpadding="0" cellspacing="0" >
                                            <tr>
                                                <td valign="top" align="left" width="240px">
                                                    <asp:Panel ID="PanelTree" runat="server"  Height="400px"  CssClass="treeSelectedNode"  BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px" width="240px" ScrollBars="Auto">
                                                        <asp:Button ID="BtnUpdateRHS" style="display:none;" runat="server" onclick="AddUserstoList"/>
                                                        <asp:TreeView ID="treeList1" runat="server"
                                                        CollapseImageUrl="image/expand1.png" ExpandImageUrl="image/collapse1.png" 
                                                        ShowExpandCollapse="true" ShowLines="true"
                                                        OnSelectedNodeChanged="treeList1_SelectedNodeChanged">
                                                        <SelectedNodeStyle CssClass="name" BackColor="Yellow" /><%--ZD 100540--%>
                                                            <Nodes>
                                                                <asp:TreeNode text="<%$ Resources:WebResources, LDAPImport_LDAP%>" value="0"></asp:TreeNode>
                                                            </Nodes>
                                                        </asp:TreeView>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" width="70%">
                                        <asp:Panel ID="PanelRooms" runat="server"  Height="400px"  CssClass="treeSelectedNode"  BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px">
                                           <table border="0" cellpadding="0" cellspacing="0" width="99%">
                                              <tr>
                                                <td width="100%">
                                                    <span  class="blackblodtext"> <font size="1"><asp:Literal Text="<%$ Resources:WebResources, LDAPImport_Clickonthehe%>" runat="server"></asp:Literal></font></span>
                                                    <asp:CheckBox ID="chkSelectAll" CheckedChanged="chkSelectAll_CheckedChanged" AutoPostBack="true"  runat="server"  title="Select/Unselect All" onclick="grd1.SelectAllRowsOnPage(this.checked);javascript:DisableCombo();"  style="vertical-align:middle;"/>
                                                    <label><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ManageCustomAttribute_ChkAll%>" runat="server"></asp:Literal></label>
                                                    <%--<input onclick="grd1.UnselectAllRowsOnPage();" type="checkbox" value="Unselect All on Page" /><label> Unselect All</label>--%>
                                                    <input type="hidden" id="hdnSelNode" runat="server"/>
                                                    <input runat="server" id="selectedlocframe" type="hidden" />
                                                    <input type="hidden" id="clrSelgrd" runat="server"/> <%--Fixes--%>
                                                    <input type="hidden" id="hdnSelVal" runat="server"/>                                                                                                        
                                                     <input type="hidden" id="addroom" value="0" runat="server" />
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                                                        <tr>
                                                            <td width="100%" valign="top">
                                                                <%--<asp:Panel ID="grdPnl" runat="server" ScrollBars="Auto" Height="370px">--%>
                                                                        <table border="2" cellpadding="3" cellspacing="0" width="100%">
                                                                          <tr>
                                                                                <td width="100%" align="left" style="font-weight: bold; font-size: small; color: green; font-family: arial" valign="middle"> 
                                                                                        <dxwgv:ASPxGridView ID="grd1" runat="server" Width="100%" KeyFieldName="Id" OnHtmlRowCreated="ASPxGridView1_HtmlRowCreated"  
                                                                           EnableRowsCache="True"  EncodeHtml="False" EnableCallBacks="False"  ClientInstanceName="grd1" KeyboardSupport="true" AccessKey="G"
                                                                             OnCustomCallback="grd1_CustomCallback"  OnDataBound="Grid_DataBound" AllowSort="true" >
                                                                              <Styles>
                                                                                <EmptyDataRow CssClass="templateCaption" />
                                                                                <AlternatingRow Enabled="true"/>
                                                                                        
                                                                              </Styles>
                                                                             <Settings ShowVerticalScrollBar="true" VerticalScrollableHeight="300" VerticalScrollBarStyle="Standard" ShowHeaderFilterButton="false" ShowFilterRowMenu="false"/>
                                                                             <SettingsText EmptyDataRow="<%$ Resources:WebResources, NoUsersforSelectedGroup%>" HeaderFilterShowAll="false" />
                                                                             <SettingsPager Mode="ShowPager"  PageSize="100" AlwaysShowPager="true"  Position="Top"/> <%--ZD 100540--%>
                                                                             <Columns>                                                                                
                                                                                <dxwgv:GridViewDataColumn FieldName="Id" Visible="false"/>
                                                                                <dxwgv:GridViewDataColumn width="20%" FieldName="samaccountname" Caption="<%$ Resources:WebResources, LDAPImport_Login%>" VisibleIndex="1" Visible="true" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="center" />
                                                                                <dxwgv:GridViewDataColumn width="25%" FieldName="Givenname" Caption="<%$ Resources:WebResources, LDAPImport_LastName%>" VisibleIndex="2" Visible="true"  HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="center"/>
                                                                                <dxwgv:GridViewDataColumn width="25%" FieldName="sn" Caption="<%$ Resources:WebResources, LDAPImport_FirstName%>" VisibleIndex="3" Visible="true" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="center"/>
                                                                                <dxwgv:GridViewDataColumn width="25%" FieldName="mail" Caption="<%$ Resources:WebResources, LDAPImport_Email%>" VisibleIndex="4" Visible="true" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="center"/>
                                                                                <%--<dxwgv:GridViewCommandColumn width="25%" ShowSelectCheckbox="True" VisibleIndex="5">
                                                                                     <HeaderTemplate>
                                                                                         <input type="checkbox" onclick="grd1.SelectAllRowsOnPage(this.checked);Addusers(this.checked);" style="vertical-align:middle;" title="Select/Unselect all rows on the page"></input>
                                                                                         <asp:CheckBox Text="Select User onclick="grd1.SelectAllRowsOnPage(this.checked);" &nbsp;" style="vertical-align:middle;" title="Select/Unselect all rows on the page"/>
                                                                                     </HeaderTemplate>
                                                                                     <HeaderStyle Paddings-PaddingTop="1" Paddings-PaddingBottom="1" HorizontalAlign="Center"/>
                                                                                </dxwgv:GridViewCommandColumn>--%>

                                                                                
                                                                              </Columns>
                                                                             <SettingsDetail ShowDetailRow="true" ShowDetailButtons="true" AllowOnlyOneMasterRowExpanded="true"/>
                                                                             
                                                                             <Templates>
                                                                                 <DataRow>
                                                                                <div style="padding:5px">                                                                                     
                                                                                         <table align="center" class="templateTable" cellpadding="3" cellspacing="1" width="99%" border="0">
                                                                                          <tr> <%--FB 2320 - Starts--%>
                                                                                                 <td class="templateCaption" style="width:20%"> <%--Edited for FF--%>
                                                                                                     <span style="font-weight:bold"><asp:Literal Text="<%$ Resources:WebResources, LDAPImport_Login%>" runat="server"></asp:Literal></span>
                                                                                                    <%#  DataBinder.Eval(Container, "DataItem.samaccountname")%>
                                                                                                 </td>
                                                                                                 <td class="templateCaption" style="width:25%">
                                                                                                    <span style="font-weight:bold"><asp:Literal Text="<%$ Resources:WebResources, LDAPImport_LastName%>" runat="server"></asp:Literal></span><%--Edited for FF--%>
                                                                                                    <%#  DataBinder.Eval(Container, "DataItem.sn")%>
                                                                                                  </td>
                                                                                                 <td class="templateCaption" style="width:25%">
                                                                                                    <span style="font-weight:bold"><asp:Literal Text="<%$ Resources:WebResources, LDAPImport_FirstName%>" runat="server"></asp:Literal></span> <%--Edited for FF--%>
                                                                                                    <%#  DataBinder.Eval(Container, "DataItem.Givenname")%>
                                                                                                 </td>
                                                                                                  <td class="templateCaption" rowspan="2" align="center" valign="middle" style="width:25%" >
                                                                                                    <asp:HyperLink NavigateUrl="#" ID="Hyper1" Text="<%$ Resources:WebResources, LDAPImport_Hyper1%>" style="cursor:pointer;color:Green;" runat="server"></asp:HyperLink> <%--ZD 100369--%>
                                                                                                    <%--<asp:CheckBox ID="Hyper1"  style="cursor:pointer;color:Green;" runat="server"/>--%>
                                                                                                 </td>
                                                                                             </tr>
                                                                                             <tr>
                                                                                                 <td class="templateCaption" style="width:70%" nowrap="nowrap" colspan="3">
                                                                                                     <span style="font-weight:bold"><asp:Literal Text="<%$ Resources:WebResources, LDAPImport_Email%>" runat="server"></asp:Literal></span>
                                                                                                    <%#  DataBinder.Eval(Container, "DataItem.mail")%>
                                                                                                 </td>
                                                                                             </tr> <%--FB 2320 - End--%>
                                                                                         </table>
                                                                                      </div>   
                                                                                 </DataRow>
                                                                                 <DetailRow>
                                                                                     <div style="padding:5px">                                                                                 
                                                                                         <table align="center" class="templateTable" cellpadding="1" cellspacing="1" width="100%" border="0">
                                                                                             <tr>
                                                                                                <td class="templateCaption"><span style="font-weight:bold"><asp:Literal Text="<%$ Resources:WebResources, LDAPImport_SchemaEntry%>" runat="server"></asp:Literal></span><%# Eval("SchemaEntry")%></td>
                                                                                                <td class="templateCaption"><span style="font-weight:bold"><asp:Literal Text="<%$ Resources:WebResources, LDAPImport_CountryCode%>" runat="server"></asp:Literal></span><%# Eval("countrycode")%></td>
                                                                                                <td class="templateCaption"><span style="font-weight:bold"><asp:Literal Text="<%$ Resources:WebResources, LDAPImport_WhenCreated%>" runat="server"></asp:Literal></span><%# Eval("whencreated")%></td>
                                                                                                <td class="templateCaption"><span style="font-weight:bold"><asp:Literal Text="<%$ Resources:WebResources, LDAPImport_WhenChanged%>" runat="server"></asp:Literal></span><%# Eval("whenchanged")%></td>
                                                                                             </tr>
                                                                                             <tr>
                                                                                                <td class="templateCaption" colspan="4"><span style="font-weight:bold"><asp:Literal Text="<%$ Resources:WebResources, LDAPImport_DistinguishedNa%>" runat="server"></asp:Literal></span><%# Eval("distinguishedName")%></td>
                                                                                             </tr>
                                                                                         </table>
                                                                                      </div>   
                                                                                 </DetailRow>
                                                                             </Templates>
                                                                         </dxwgv:ASPxGridView>
                                                                                </td>
                                                                            </tr>
                                                                         </table>
                                                                <%--</asp:Panel>--%>
                                                            </td>
                                                         </tr>
                                                     </table>
                                                 </td>
                                            </tr>
                                         </table>
                                      </asp:Panel>
                                   </td>                                         
                                    <td  style="vertical-align:top" width="18%" id="TDSelectedUser" runat="server" align="right">
                                        <asp:Panel ID="SelectedUsers" runat="server"  Height="400px" Width="99%" CssClass="treeSelectedNode" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr class="tableHeader">
                                                        <td><asp:Literal Text="<%$ Resources:WebResources, LDAPImport_SelectedUsers%>" runat="server"></asp:Literal></td>
                                                    </tr>
                                                    <tr>
                                                    <td align="left">
                                                        <a href="" onclick="this.childNodes[0].click();return false;"><img border='0' src='image/deleteall.gif' alt="Delete All"  style="cursor:pointer;" title="<%$ Resources:WebResources, LDAPImport_RemoveAll%>" runat="server" id="ImageDel" width='18' height='18' onClick="JavaScript:ClearAllSelection()"/> <%-- FB 2798 ZD 100429--%> <%-- ZD 100419--%>  
                                                        <span  class="treeRootNode"  onClick="JavaScript:ClearAllSelection()"><asp:Literal Text="<%$ Resources:WebResources, LDAPImport_RemoveAll%>" runat="server"></asp:Literal></span></a> <%--ZD 100369--%>
                                                    </td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td>
                                                            <asp:Panel ID="pnlUsrGrd" runat="server" ScrollBars="Auto"  Height="350px">
                                                                <asp:DataGrid  ShowHeader="false" Width="100%" ID="SelectedGrid" runat="server" AutoGenerateColumns="False" OnItemDataBound="SetUserAttributes" Font-Names="Verdana" Font-Size="Small" >
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="Id" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="samaccountname" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="sn" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="GivenName" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="mail" Visible="false"></asp:BoundColumn>
                                                                        <asp:TemplateColumn>
                                                                            <ItemTemplate>
                                                                                <table width="100%" cellspacing="3">
                                                                                    <tr>
                                                                                        <td align="left" width="90%" runat="server" id="usrName"></td>
                                                                                        <td align="left" width="10%" ><a href="" onclick="this.childNodes[0].click();return false;"><img border='0' style="cursor:pointer;" src='image/btn_delete.gif' alt="Delete" runat="server" id="ImageDel" width='18' height='18' title="<%$ Resources:WebResources, Delete%>"/></a></td><%-- ZD 1000419--%>
                                                                                    </tr>
                                                                                </table>   
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                               </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>   
                        </ContentTemplate>
                     </asp:UpdatePanel>  
                     </td></tr></table> 
                 </td>
            </tr>
<%--PSU End--%>
            <tr>
                <td>
                    <table cellpadding="3" cellspacing="0" width="100%" border="0" style="border-color:Blue;border-width:1px;border-style:Solid;"> 
                         <tr class="tableBody">
                            <td align="right">
                                <table width="90%" border="0" cellpadding="3" cellspacing="0">
                                    <tr>
                                        <td align="right" class="blackblodtext" width="50%">
                                            <asp:Literal Text="<%$ Resources:WebResources, LDAPImport_ApplyUserTemp%>" runat="server"></asp:Literal>
                                            <asp:DropDownList ID="lstLDAPTemplates" runat="server" DataValueField="ID" DataTextField="name" CssClass="altText"/>
                                        </td>
                                        <td align="center">
                                            <button ID="btnSubmitUsers" runat="server" ValidationGroup="Submit" style="width:160px" 
                                             onclick="DataLoading(1);"  onserverclick="btnSubmitUsersClick">
													<asp:Literal Text="<%$ Resources:WebResources, LDAPImport_btnSubmitUsers%>" runat="server"></asp:Literal></button> <%--FB 2320--%> <%--FB 2664--%>  <%--ZD 100429--%> <%--ZD 100369--%> 
                                        </td>
                                    </tr>
                                 </table>
                            </td>
                        </tr>
                    </table>
                    <%--FB 2320 - Starts--%>
                    <asp:Button ID="LdapTrigger" runat="server" Style="display: none;" />
                    <ajax:ModalPopupExtender ID="LdapPopUp" runat="server" TargetControlID="LdapTrigger"
                        PopupControlID="PopupLdapPanel" DropShadow="false" Drag="true" BackgroundCssClass="modalBackground"
                        CancelControlID="ClosePUp" BehaviorID="LdapTrigger">
                    </ajax:ModalPopupExtender>
                    <asp:Panel ID="PopupLdapPanel" runat="server" Width="60%" Height="80%" HorizontalAlign="Center"
                        CssClass="treeSelectedNode" ScrollBars="None">
                        <table align="center" cellpadding="3" cellspacing="0" width="95%">
                            <tr style="height: 50%">
                                <td>
                                    <br />
                                    <input align="middle" type="button" runat="server" validationgroup="Submit1" id="ClosePUp"
                                        value=" Close " class="altShortBlueButtonFormat" />
                                    <asp:Button ID="btnSubmit" runat="server" Text="<%$ Resources:WebResources, LDAPImport_btnSubmit%>" ValidationGroup="Submit1"
                                        class="altShortBlueButtonFormat" OnClick="btn_Submit" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Panel ID="PopupLdapPanel1" runat="server" Width="100%" Height="100%" HorizontalAlign="Center"
                                        CssClass="treeSelectedNode" ScrollBars="Vertical">
                                        <div align="left" id="conftypeDIV" runat="server" style="position:relative; top:0px; left:0px; overflow:auto;width: 100%; height: 100%; border: none;" >
                                            <table >
                                                <asp:DataGrid ID="LdapListTbl" runat="server" BorderColor="Blue" Width="100%" CellPadding="4"
                                                    BorderStyle="Solid" BorderWidth="1" AutoGenerateColumns="false" Style="border-collapse: separate" >
                                                    <SelectedItemStyle CssClass="tableBody" />
                                                    <AlternatingItemStyle CssClass="tableBody" />
                                                    <ItemStyle CssClass="tableBody" />
                                                    <HeaderStyle CssClass="tableHeader" Height="10px" />
                                                    <EditItemStyle CssClass="tableBody" />
                                                    <FooterStyle CssClass="tableBody" />
                                                    <Columns>
                                                        <asp:BoundColumn DataField="samaccountname" HeaderStyle-Width="30%" HeaderText="Login"
                                                            HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="cn" HeaderText="Username" HeaderStyle-Width="30%" HeaderStyle-CssClass="tableHeader"
                                                            ItemStyle-CssClass="tableBody"></asp:BoundColumn><%--ZD 102706--%>
                                                        <asp:TemplateColumn HeaderText="Email Id" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtUserEmail" CssClass="altText" Width="80%" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mail") %>'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="reqUserEmail" ControlToValidate="txtUserEmail" ValidationGroup="Submit1"
                                                                    Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="regEmail1_1" ControlToValidate="txtUserEmail"
                                                                    Display="dynamic" runat="server" ValidationGroup="Submit1" ErrorMessage="<%$ Resources:WebResources, Invalidemail%>"
                                                                    ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                                                <asp:RegularExpressionValidator ID="regEmail1_2" ControlToValidate="txtUserEmail"
                                                                    Display="dynamic" ValidationGroup="Submit1" runat="server" SetFocusOnError="true"
                                                                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters14%>"
                                                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </table>
                                        </div>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                        <br />
                    </asp:Panel>
                    <%--FB 2320 - End--%>
                </td>
            </tr>
       </table>  
   </form>
<script type="text/javascript" src="inc/softedge.js"></script>
<script type="text/javascript">

var roomNamesStr,roomIdsStr

function Addusers()
{
CorrectHdnString();

var args = Addusers.arguments;

 var locs =  document.getElementById("selectedlocframe");
 var adlocs =  document.getElementById("addroom");

roomIdsStr = locs.value.split('�'); //�- Alt789(� instead of comma,) - FB 2320 


    if(!Loccontains(roomIdsStr,args[0]))
    {
        if(locs.value == "")
            locs.value = args[0];
        else
            locs.value += "�"+args[0]; //�- Alt789 - FB 2320 
            
        if(adlocs)
        adlocs.value = "1";    
      var refrsh = document.getElementById("BtnUpdateRHS");
      if(refrsh)
        refrsh.click();        
    } 
    else
    {
        alert(LdapUser);    
    }
    
}

function CorrectHdnString()
{
    var locs =  document.getElementById("selectedlocframe");
    var vlue = "";
    
    roomIdsStr = locs.value.split('�'); //�- Alt789 - FB 2320 
    
    var i = roomIdsStr.length;
        
          while (i--) 
          {
            if (roomIdsStr[i] != "") 
            {
                if(vlue == "")
                    vlue =  roomIdsStr[i].trim();
                else
                    vlue += "�"+  roomIdsStr[i].trim(); //�- Alt789 - FB 2320 
            }
            
         }
         
         locs.value = vlue;

}


function Delrooms()
{

CorrectHdnString();
var args = Delrooms.arguments;
var locs =  document.getElementById("selectedlocframe");
 var adlocs =  document.getElementById("addroom");


roomIdsStr = locs.value.split('�');//�- Alt789 - FB 2320 

    if(Loccontains(roomIdsStr,args[0]))
    {
        var i = roomIdsStr.length;
        
          while (i--) 
          {
            if (roomIdsStr[i] == args[0]) 
            {
               roomIdsStr[i] = ""; 
            }
         }
         
        i = roomIdsStr.length;
        locs.value = ""; 
         while (i--) 
         {
            if (roomIdsStr[i] != "") 
            {
             if(locs.value == "")
                locs.value =roomIdsStr[i];
            else
                locs.value += "�"+roomIdsStr[i];//�- Alt789 - FB 2320 
            }
          }
    }
     if(adlocs)
        adlocs.value = "1";
    var refrsh = document.getElementById("BtnUpdateRHS");
    if(refrsh)
      refrsh.click();  
}


function ClearAllSelection()
 {
    try 
    {
        var locs =  document.getElementById("selectedlocframe");
        var adlocs =  document.getElementById("addroom");
        locs.value = "";
        if(adlocs)
           adlocs.value = "1";
        
        var refrsh = document.getElementById("BtnUpdateRHS");
        if(refrsh)
           refrsh.click();
    }
    catch(exception)
    {
      alert("in");
    }
 }
 

function Loccontains(a, obj) {
  var i = a.length;  
  while (i--) {
    if (a[i] === obj) {
      return true;
    }
  }
  return false;
}


function DataLoading(val)
{
    if (val=="1")
//        document.getElementById("dataLoadingDIV").innerHTML="<b><font color='#FF00FF' size='2'>Data loading ...</font></b>&nbsp;&nbsp;&nbsp;&nbsp;<img border='0' src='image/wait1.gif' width='100' height='12'>";
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678

    else
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678                 
}
var prm = Sys.WebForms.PageRequestManager.getInstance();
prm.add_initializeRequest(initializeRequest);

prm.add_endRequest(endRequest);

var postbackElement;
  
function initializeRequest(sender, args) 
{
    document.body.style.cursor = "wait";
    DataLoading(1);

}

function endRequest(sender, args) 
{
    document.body.style.cursor = "default";
    DataLoading(0);    
}
document.getElementById('PanelTree').childNodes[3].tabIndex = "-1"; //ZD 100369
//--></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>
