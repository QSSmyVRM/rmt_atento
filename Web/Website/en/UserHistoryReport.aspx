<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_MyVRM.UserHistoryReport" %>

<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<script type="text/javascript">
  var servertoday = new Date();
</script>
<script type="text/javascript" language="javascript" src='script/lib.js'></script>
<script type="text/javascript" src="script/calview.js"></script>
<script type="text/javascript" src="inc/functions.js"></script>
<script type="text/javascript" language="javascript">

    var rtn;
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
    function fnSubmit()
        {
          DataLoading(1); // ZD 100176
          if (document.form1.txtFromDate.value != "" && document.form1.txtToDate.value == "" ) {
              alert(UserToDate);
              DataLoading(0);//ZD 103881
              document.form1.txtToDate.focus();
              return false;
          }
          else if (document.form1.txtToDate.value != "" && document.form1.txtFromDate.value == "") {
              alert(UserFromDate);
              DataLoading(0); //ZD 103881
              document.form1.txtFromDate.focus();
              return false;
          } //ZD 103881 start
          else if (document.form1.txtToDate.value == "" && document.form1.txtFromDate.value == "") {
              alert(UserFromToDate);
              DataLoading(0);
              document.form1.txtFromDate.focus();
              return false;
          } //ZD 103881 End
                textValue = document.form1.txtFromDate.value;
                index = textValue.length;
                startDate = textValue.substring(0,index);
                textValue = document.form1.txtToDate.value;
                index = textValue.length;
                endDate = textValue.substring(0, index);

                //ZD 103305 START
                if ('<%=Session["FormatDateType"]%>' == "dd/MM/yyyy") {
                    var dateVal = startDate.split('/');
                    startDate = dateVal[1] + "/" + dateVal[0] + "/" + dateVal[2];
                    var dateVal2 = endDate.split('/');
                    endDate = dateVal2[1] + "/" + dateVal2[0] + "/" + dateVal2[2];
                }
                //ZD 103305 END

                             
                var compareVal = fnCompareDate(startDate,endDate)
                if(compareVal == "L")
                {
                    alert(UserDate);
                    DataLoading(0); //ZD 103881
                    return false;
                }  
                
                return true;      
        }
        function ChangeStartDate(frm)
        {
            var confenddate = '';
            confenddate = GetDefaultDate(document.getElementById("txtToDate").value,'<%=format%>');
            
            var confstdate = '';
            confstdate = GetDefaultDate(document.getElementById("txtFromDate").value,'<%=format%>');
            
            if(document.getElementById("txtToDate").value != "")
                reqtodate.style.display = 'none';
                
            if (Date.parse(confstdate) > Date.parse(confenddate))
            {
                if (frm == "0") 
                {
                    alert(UserDate);
                    rtn = false;
                    return false;
                }
            }
            else
            {
                rtn = true;
                return true;
            }
        }
        function ChangeEndDate(frm)
        {
            var confenddate = '';
            if(document.getElementById("txtFromDate").value != "")
                confenddate = GetDefaultDate(document.getElementById("txtToDate").value,'<%=format%>');
                
            var confstdate = '';
            if(document.getElementById("txtFromDate").value != "")
                confstdate = GetDefaultDate(document.getElementById("txtFromDate").value,'<%=format%>');
                
            if(document.getElementById("txtFromDate").value != "")
                reqfromdate.style.display = 'none';
            
            if (Date.parse(confenddate) < Date.parse(confstdate))
            {
                if (frm == "0") 
                {
                    alert(UserDate);
                    rtn = false;
                    return false;
                }
            }
            else
            {
                rtn = true;
                return true;
            }
        }
        function fnCancel() //FB 2565
        {

            window.location.replace('SuperAdministrator.aspx');  //CSS Project
            return false; //ZD 100420
        }
        //ZD 100176 start
        function DataLoading(val) {
            if (val == "1")
                document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
            else
                document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
        }
        //ZD 100176 End


   </script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>User Login History Report</title>
    <script type="text/javascript" src="script/cal-flat.js"></script>
    <script type="text/javascript" src="../<%=Session["language"] %>/lang/calendar-en.js"></script>
    <script type="text/javascript" src="script/calendar-setup.js"></script>
    <script type="text/javascript" src="script/calendar-flat-setup.js"></script>
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1982--%>
    
</head>
<body>
    <form id="form1" runat="server">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <center>
        <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
    </center>
    
    <h3 style="text-align: center"><asp:Literal Text="<%$ Resources:WebResources, UserHistoryReport_UserLoginHist%>" runat="server"></asp:Literal></h3>
            <br /><br />  
            <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                <img border='0' src='image/wait1.gif' alt='Loading..' />
            </div><%--ZD 100678 End--%>
    <div id ="divheight"><%--FB 2875 Start--%> <%--ZD 100393--%>
    <table width="100%" border="0">
    <tr><td>
        <table width="95%" align="right" bgcolor="white" cellpadding="1" cellspacing="0" border ="0"> <%-- FB 2050 --%>
        <tr id="trSearchH" runat="server">
                <td align="Left">
                                <span class="subtitleblueblodtext" style=" margin-left:-20px"><asp:Literal Text="<%$ Resources:WebResources, UserHistoryReport_SearchUsers%>" runat="server"></asp:Literal></span>
                            
                </td>
            </tr>
            <tr>
                             <td align="left" style="font-weight:bold; width:13%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserHistoryReport_FirstName%>" runat="server"></asp:Literal></td> <%-- FB 2050 --%>
                            <td align="left" style="width:20%"> <%-- FB 2050 --%>
                                <asp:TextBox ID="txtFirstName" CssClass="altText" runat="server"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="regfirstname" ControlToValidate="txtFirstName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters43%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td align="left" style="font-weight:bold; width:13%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserHistoryReport_LastName%>" runat="server"></asp:Literal></td> <%-- FB 2050 --%>
                            <td align="left" style="width:20%"> <%-- FB 2050 --%>
                                <asp:TextBox ID="txtLastName" CssClass="altText" runat="server"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="reglastname" ControlToValidate="txtLastName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters43%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td align="left" style="font-weight:bold; width:13%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserHistoryReport_EmailAddress%>" runat="server"></asp:Literal></td> <%-- FB 2050 --%>
                                <td align="left" style="width:20%"> <%-- FB 2050 --%>
                                    <asp:TextBox ID="txtLogin" CssClass="altText" runat="server"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="reglogin" ControlToValidate="txtLogin" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters44%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>
                        </tr>
                         <tr style="height:40px">
                                    <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserHistoryReport_DateFrom%>" runat="server"></asp:Literal></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="altText" EnableViewState="true" ></asp:TextBox>
                                        <a href="" onclick="this.childNodes[0].click();return false;"><img src="image/calendar.gif" border="0"  alt ="Date Selector" width="20" height="20" id="cal_triggerd" style="cursor: pointer;vertical-align:middle" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onblur="javascript:ChangeStartDate(0)" onclick="return showCalendar('<%=txtFromDate.ClientID %>', 'cal_triggerd', 0, '<%=format%>');" /> <%--ZD 100419--%>
                                    </td>
                                    <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, UserHistoryReport_DateTo%>" runat="server"></asp:Literal></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="altText" EnableViewState="true" onblur="javascript:ChangeEndDate(0)" ></asp:TextBox>
                                        <a href="" onclick="this.childNodes[0].click();return false;"><img src="image/calendar.gif" border="0" width="20" alt ="Date Selector" height="20" id="cal_trigger1" style="cursor: pointer;vertical-align:middle" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onblur="javascript:ChangeEndDate(0)" onclick="return showCalendar('<%=txtToDate.ClientID %>', 'cal_trigger1', 0, '<%=format%>');" /> <%--ZD 100419--%>                                              
                                    </td>
                                    <td align="center" class="blackblodtext" colspan="2">
                                         <asp:Button id="btnSearch" runat="server" text="<%$ Resources:WebResources, UserHistoryReport_btnSearch%>" cssclass="altMedium0BlueButtonFormat" validationgroup="DateSubmit" onclick="btnSearch_Click" onclientclick="javascript:return fnSubmit() "></asp:Button>
                                         
                                    
                                    
                                    <%--<input type="button" id="Close" class="altMedium0BlueButtonFormat" value="Cancel" onclick="fnCancel();javascript:DataLoading('1');" /> <%--FB 2565--%> <%--ZD 100176 --%><%--ZD 100420--%>
                                    <button id="btnCancel" class="altMedium0BlueButtonFormat"  onclick="javascript:return fnCancel();javascript:DataLoading('1');"> 
                                    <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, Cancel%>" runat="server"></asp:Literal></button><%--ZD 100420--%><%--ZD 100369--%>
                                    </td>
                                </tr>
                                 <tr>
                            <td>
                            </td>
                            <%--103624--%>
                            <td>
                                <asp:RegularExpressionValidator ID="reqfromdate" runat="server" ControlToValidate="txtFromDate" ValidationGroup="Submit"
                                    SetFocusOnError="true" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidDate2%>"
                                    ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                            </td>
                            <td>
                            </td>
                            <%--103624--%>
                            <td>
                                <asp:RegularExpressionValidator ID="reqtodate" runat="server" ControlToValidate="txtToDate" ValidationGroup="Submit"
                                    SetFocusOnError="true" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidDate2%>"
                                    ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                                </table>
         </td></tr>
         <tr><td>
                     <table width="100%" align="center">          
                    <tr>
                    
                <td align="center" width="100%">
                        <asp:DataGrid id="dgUserLoginReport" Width="90%" HorizontalAlign="Center" runat="server" AllowPaging="True" PageSize="15" PagerStyle-Mode="NumericPages" PagerStyle-Horizontalalign="right"
                            PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev" BorderColor="blue" BorderWidth="1" HeaderStyle-HorizontalAlign="Center" OnPageIndexChanged="dgUserLoginReport_PageIndexChanging" 
                            OnSortCommand="dgUserLoginReport_Sorting" GridLines="None" CellPadding="3" CellSpacing="0" HeaderStyle-Font-Bold="true" AutoGenerateColumns="false" CssClass="tableBody">
                            <SelectedItemStyle  CssClass="tableBody"/>
                            <AlternatingItemStyle CssClass="tableBody" />
                            <ItemStyle CssClass="tableBody" HorizontalAlign="Center"  />
                            <HeaderStyle CssClass="tableHeader" Height="30px" />
                            <EditItemStyle CssClass="tableBody" />
                                    <%--Window Dressing--%>
                            <FooterStyle CssClass="tableBody"/>
                            <Columns>
                                <asp:BoundColumn DataField="FirstName" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, UserHistoryReport_FirstName%>" SortExpression="1"></asp:BoundColumn>
                                <asp:BoundColumn DataField="LastName" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, UserHistoryReport_LastName%>" SortExpression="2"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Email" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, Allocation_Email%>" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="LoginDateTime" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, LoginDateTime%>" SortExpression="3"></asp:BoundColumn>
                            </Columns>
                    </asp:DataGrid>
                    <asp:Label runat="server" ID="lblusrloginrecord" Visible="false"  CssClass="lblError"></asp:Label>
                </td>
            </tr>
            </table>
            </td></tr>
            </table>
            <%--FB 2875 End--%>
         </div>
         <asp:TextBox ID="txtSortBy" runat="server" Visible="false"></asp:TextBox>
    </form>
</body>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<%--ZD 100420--%>
<script type="text/javascript" >
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };

    document.getElementById('cal_trigger1').setAttribute("onblur", "document.getElementById('btnSearch').focus()");
    document.getElementById('btnSearch').setAttribute("onfocus", "");
    document.getElementById('btnSearch').setAttribute("onblur", "document.getElementById('Close').focus()");
    //ZD 100393 start
    if (document.getElementById("lblusrloginrecord") != null && document.getElementById("lblusrloginrecord").innerHTML != "") ///103881
           document.getElementById("divheight").style.height="400px"
    //ZD 100393 start
</script>
<%--ZD 100420--%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" --> 
</html>
