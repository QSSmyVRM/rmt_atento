/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100886 End*/
var client
function change_display_layout_prompt(promptpicture, prompttitle, epid, epty, dl, rowsize, images, imgpath) 
{
	var title = new Array()
	title[0] = "Default ";
	title[1] = "Custom ";
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");

	promptbox.position = 'absolute'
	promptbox.top = -100+mousedownY;
	promptbox.left = mousedownX - 30;
	promptbox.width = rowsize * 88 
	promptbox.border = 'outset 1 #bbbbbb' 

	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='titlebar'><img src='" + promptpicture + "' height='18' width='18'></td><td class='titlebar'>" + prompttitle + "</td></tr></table>" 
	m += "<table cellspacing='2' cellpadding='2' border='0' width='100%' class='promptbox'>";

	if (typeof(document.getElementById("client")) != "undefined")
		client = document.getElementById("client").value;
	else 
		client = "";
//alert(client);
	if ( (client == "BTBOCES") || (client == "WHOUSE") )
  	  for (s=0;s<2;s++) {
		imagesary = images.split("|")[s].split(":")
		if (s==1)
		{
			path = imgpath + client + "/";
		}	
		else
			path = imgpath;
	rowNum = parseInt( (imagesary.length + rowsize - 2) / rowsize, 10 );
		m += "	<tr>";
		m += "    <td colspan='" + (rowsize * 2) + "' align='left'><b>" + title[s] + "Display Layout</b><input name='default" + s + "' id='default" + s + "' type='checkbox' onclick='javascript:displayLayout(" + s + ")'></td>";
		m += "  </tr>"
		m += "	<tr><td>";
		m += "  <div name='display" + s + "' id='display" + s + "' style='display:None'>";
		m += "  <table>";
		m += "  <tr>"
		m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
		m += "  </tr>"
	
		imgno = 0;
		for (i = 0; i < rowNum; i++) {
			m += "  <tr>";
			for (j = 0; (j < rowsize) && (imgno < imagesary.length-1); j++) {
				
			
				m += "    <td valign='middle'>";
				m += "      <input type='radio' name='layout' id='layout' value='" + imagesary[imgno] + "' onClick='chgimg(\"" + epid + "\", \"" + epty + "\", \"" + imagesary[imgno] + "\", \"" + imgpath + "\")'" + ( (dl == imagesary[imgno]) ? " checked" : "" ) +">";
				m += "    </td>";
				m += "    <td valign='middle'>";
				m += "      <img src='" + path + imagesary[imgno] + ".gif' width='57' height='43' alt='Layout'>"; //ZD 100419
				m += "    </td>";
				imgno ++;
			}
			m += "  </tr>";
		}
		m += " </table></div></td><tr>";
	}	
	else {
		imagesary = images.split(":");
		rowNum = parseInt( (imagesary.length + rowsize - 2) / rowsize, 10 );
		m += "	<tr>";
		m += "    <td colspan='" + (rowsize * 2) + "' align='left'><b>Display Layout</b></td>";
		m += "  </tr>"
		m += "  <tr>"
		m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
		m += "  </tr>"
	
		imgno = 0;
		for (i = 0; i < rowNum; i++) {
			m += "  <tr>";
			for (j = 0; (j < rowsize) && (imgno < imagesary.length-1); j++) {
				
			
				m += "    <td valign='middle'>";
				m += "      <input type='radio' name='layout' id='layout' value='" + imagesary[imgno] + "' onClick='chgimg(\"" + epid + "\", \"" + epty + "\", \"" + imagesary[imgno] + "\", \"" + imgpath + "\")'" + ( (dl == imagesary[imgno]) ? " checked" : "" ) +">";
				m += "    </td>";
				m += "    <td valign='middle'>";
				m += "      <img src='" + imgpath + imagesary[imgno] + ".gif' width='57' height='43' alt='Layout'>"; //ZD 100419
				m += "    </td>";
				imgno ++;
			}
			m += "  </tr>";
		}
	}	

	m += "  <tr>";
	m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
	m += "  </tr>"
	m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
	m += "    <input type='button' class='altMedium0BlueButtonFormat' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(\"" + epid + "\", \"" + epty + "\");'>"
	m += "    <input type='button' class='altMedium0BlueButtonFormat' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
	m += "  </td></tr>"
	m += "</table>" 
	
	document.getElementById('prompt').innerHTML = m;
} 


function displayLayout(s)
{	
	var obj = document.getElementById("default" + s);
	var temp = document.getElementById("display" + s);
	//alert(obj.checked);
	if (obj.checked)
		temp.style.display = "block";
	else
		temp.style.display = "none";
} 

function chgimg(epid, epty, img, imgpath)
{
//	document.getElementById("layoutimg" + epid + "-" + epty).src = imgpath + img + ".gif";
}


function saveOrder(epid, epty) 
{
	newlayout = "";
	
	cb = document.getElementsByName ("layout")
	if (cb.length == 1) {
		if (cb.checked)
			newlayout = cb.value;
	} else {
		for (i = 0; i < cb.length; i++) {
			if (cb[i].checked) {
				newlayout = cb[i].value;
				break;
			}
		}
	}
	
	document.frmTerminalcontrol.displaylayout.value = newlayout;
	document.frmTerminalcontrol.epid.value = epid;
	document.frmTerminalcontrol.terminaltype.value = epty;
	document.frmTerminalcontrol.cmd.value = "DisplayTerminal";
	
	document.frmTerminalcontrol.submit();
} 


function cancelthis()
{
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
}