<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" Inherits="en_ManageUserRoles" %>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>


<html >
<head runat="server">
    <title>myVRM</title>
    <script type="text/javascript" src="../<%=Session["language"] %>/inc/functions.js"></script> <%--ZD 101714--%>
    <script type="text/javascript" src="script/errorList.js"></script>
    <script language="javascript">
<!--

	cansubmit = true;
	newroleno = 0;
	//ZD 100604 start
	var img = new Image();
	img.src = "../en/image/wait1.gif";
	//ZD 100604 End
	// ZD 100263
	if (document.addEventListener != null) {
	    document.addEventListener("contextmenu", function(e) {
	        e.preventDefault();
	    }, false);
	}
	
	function selectRole(cb)
	{
	    
	    
		document.frmManageuserroles.RoleName.disabled = false;	
		
		document.getElementById("RNtxt").innerHTML="";
		urary = ((document.frmManageuserroles.userroles.value).split("##"))[cb.selectedIndex].split("@@");
		
		document.frmManageuserroles.roleidx.value = cb.selectedIndex;
		document.frmManageuserroles.RoleID.value = urary[0];
		document.frmManageuserroles.RoleName.value = urary[1];
		document.frmManageuserroles.MenuMask.value = urary[2];
		document.frmManageuserroles.Active.value = urary[3];
		document.frmManageuserroles.Locked.value = urary[4];
		document.frmManageuserroles.CreateType.value = urary[5]; // FB 1968
		
		ifrmMenumasklist.init();
		
		if (urary[3] == "1") {
			document.frmManageuserroles.RoleName.disabled = true;
			document.frmManageuserroles.ManageuserrolesDelete.disabled = true;
			document.frmManageuserroles.ManageuserrolesDelete.className = "btndisable"; //FB 2664
			document.getElementById("RoleMessage").innerHTML = RSAssignedRole;  // Active: role is being used by some user template(s)
			return 1;
		}
		// FB 1968 Starts
		if (urary[4] == "1")
		 {
		    if((urary[5]) =="1")
		    {
			    document.frmManageuserroles.RoleName.disabled = true;
			    document.frmManageuserroles.ManageuserrolesDelete.disabled = true;
			    document.frmManageuserroles.ManageuserrolesDelete.className = "btndisable"; //FB 2664
			    document.getElementById("RoleMessage").innerHTML = RSDefaultRoleMsg;
			    return 1;
			}
			else
			{
			    document.frmManageuserroles.RoleName.disabled = true;
			    document.frmManageuserroles.ManageuserrolesDelete.disabled = true;
			    document.frmManageuserroles.ManageuserrolesDelete.className = "btndisable"; //FB 2664
			    document.getElementById("RoleMessage").innerHTML = RSCustomRoleMsg;
			    return 1;
			}
		}
		// FB 1968 Ends 
		
		document.frmManageuserroles.RoleName.disabled = false;
		document.frmManageuserroles.ManageuserrolesDelete.disabled = false;
		document.frmManageuserroles.ManageuserrolesDelete.className = "altMedium0BlueButtonFormat"; //FB 2664
		document.getElementById("RoleMessage").innerHTML="";
	}


	function DelUserRole()
	{
		cb = document.frmManageuserroles.UserRole;
		//FB 2664 Starts
		if (document.frmManageuserroles.UserRole.selected == urary[5])
		    document.frmManageuserroles.ManageuserrolesDelete.disabled = false;
		document.frmManageuserroles.ManageuserrolesDelete.className = "altMedium0BlueButtonFormat";
		// FB 2664     End
		if (cb.selectedIndex >-1) {
		    var isRemoveGroup = confirm(UserRoleDelete)
			if (isRemoveGroup == false) {
				return;
			}
			
			ursary = (document.frmManageuserroles.userroles.value).split("##");
		
			newstr = "";
			for (i = 0; i < cb.selectedIndex; i++)
				newstr += ursary[i] + "##";
			for (i = cb.selectedIndex+1; i < ursary.length-1; i++)
				newstr += ursary[i] + "##";
			document.frmManageuserroles.userroles.value = newstr;

			cb.options[cb.selectedIndex] = null;
			cb.selectedIndex = -1;
			document.frmManageuserroles.roleidx.value = "-1";
			document.frmManageuserroles.RoleID.value = "";
			document.frmManageuserroles.RoleName.value = "";
			document.frmManageuserroles.MenuMask.value = "8*0-4*0+8*0+4*0+4*0+5*0+4*0+5*0+5*0+2*0+2*0+4*0+8*0+6*0+3*0+9*0+2*0+2*0+2*0+1*0-7*0"; //FB 1779 //FB 2023 DD2 //FB 2593 //FB 2885  ZD 101233 //ZD 101525 //ZD 102123 //ZD 102532 //ZD 100040
			ifrmMenumasklist.init();
		}

		document.frmManageuserroles.ManageuserrolesDelete.disabled = true;
		document.frmManageuserroles.ManageuserrolesDelete.className = "btndisable"; //FB 2664
		document.frmManageuserroles.RoleName.disabled = true;	
		document.getElementById("RNtxt").innerHTML="<b><font size='1'><li>Role Name field is now uneditable until one specific role in left list is selected.<li>Please see above instruction for details.</font></b>";
	}
	
	//Function added by Vivek as a fix for Issue number 267
	//This function will iterate through existing UserRoles and if it exists then won't add to the list
	function checkifUserRoleExists()
	{
	   
//	    alert(document.frmManageuserroles.RoleName.value);
//	    alert(document.frmManageuserroles.UserRole.length);
	    
	    for(i = 0;i< document.frmManageuserroles.UserRole.length-1;i++)
	    {
        //alert(document.frmManageuserroles.UserRole.options[i].text);
	        //alert(document.frmManageuserroles.RoleName.value + " (Custom)");
            if(document.frmManageuserroles.UserRole.options[i].text == document.frmManageuserroles.RoleName.value + "("+ RSCustom +")") //Latest Spanish
            {
                alert(UserRoleName);
                return false;
            }
        }
        return true;
    }

	function AddUserRole()
	{
	    //Function added by Vivek as a fix for Issue number 267
	    if(checkifUserRoleExists()== false)
	        return 0;
	        
		newroleno++;
        //Latest Spanish
		var newoption = new Option("" + NewUserRole + " " + newroleno + " (" + RSCustom + ")", "new", true, true);
		
 		cb = document.frmManageuserroles.UserRole;
		cb.options[cb.length] = newoption;

		document.frmManageuserroles.userroles.value += "new@@" + NewUserRole + " " + newroleno + "@@8*248-4*0+8*0+4*0+4*0+5*0+4*0+5*0+5*0+2*0+2*0+4*0+8*0+6*0+3*0+9*0+2*0+2*0+2*0+1*0-7*0@@0##"; //FB 1779 //FB 2023 DD2 //FB 2593 //FB 2885  ZD 101233 //ZD 101525 //ZD 102123 ZD 102532//ZD 100040
		document.frmManageuserroles.roleidx.value = cb.length-1; 
		document.frmManageuserroles.RoleID.value = "new";
		document.frmManageuserroles.RoleName.value = "" + NewUserRole + " " + newroleno + "";
		document.frmManageuserroles.MenuMask.value = "8*248-4*0+8*0+4*0+4*0+5*0+4*0+5*0+5*0+2*0+2*0+4*0+8*0+6*0+3*0+9*0+2*0+2*0+2*0+1*0-7*0"; //FB 1779 //FB 2023 DD2 //FB 2593 //FB 2885  ZD 101233 ZD 101525 //ZD 102123 //ZD 102532//ZD 100040
		document.getElementById("Active").value = "0";
		document.getElementById("Locked").value = "0";
		document.getElementById("RoleMessage").innerHTML=""; //FB 311
		ifrmMenumasklist.init(); 

		document.frmManageuserroles.RoleName.disabled = false;	
		document.getElementById("RNtxt").innerHTML="";
		document.frmManageuserroles.ManageuserrolesDelete.disabled = ( (document.frmManageuserroles.userroles.value == "") ? true : false);
		
	}
	
	
	function save () 
	{
		if (!ifrmMenumasklist.needblock) {
			idx = parseInt(document.frmManageuserroles.roleidx.value, 10);
			ursary = (document.frmManageuserroles.userroles.value).split("##");
			ursary[idx] = document.frmManageuserroles.RoleID.value + "@@" 
						+ document.frmManageuserroles.RoleName.value + "@@"
						+ document.frmManageuserroles.MenuMask.value + "@@"
						+ document.frmManageuserroles.Active.value + "@@"
						+ document.frmManageuserroles.CreateType.value + "@@"  // FB 1968
						+ document.frmManageuserroles.Locked.value;
		
			newstr = "";
			for (i = 0; i < ursary.length-1; i++) {
				newstr += ursary[i] + "##";
			}
			document.frmManageuserroles.userroles.value = newstr;
		}
	}
		
	function syn (val) 
	{
		document.frmManageuserroles.UserRole.options[parseInt(document.frmManageuserroles.roleidx.value, 10)].text = val + "(" + RSCustom + ")";//Latest Spanish
	}
	
	function chksyn (val) 
	{
		syn(val);
		if (Trim(val) == "") {
		    alert(UserRoleInput);
			document.frmManageuserroles.RoleName.focus();
			cansubmit = false;
			save ();
		} else {
		
			cansubmit = true;
			save ();
			
		}
	}


	function frmManageuserroles_Validator() {
	    
	    var sharedEnv, admin;
		//ZD 101388
	    if (CheckMenuSelection() == false)
	        return false;

	    
	    sharedEnv = '<%=Application["sharedEnv"]%>';
	    admin = '<%=Session["admin"]%>';
	    
		if ( (sharedEnv == "1") && (admin != "2"))
		{
		    alert(UserRoleFeature);
			return false;
		}
        //Added by manisha to fix the issue no 267
          for(i = 0;i< document.frmManageuserroles.UserRole.length;i++)
	    {
	        
	        for(j = 0;j< document.frmManageuserroles.UserRole.length;j++)
	       {	         
	           if(i!=j)
	           { 
                   if(document.frmManageuserroles.UserRole.options[i].text == document.frmManageuserroles.UserRole.options[j].text)
                    {
                        alert(UserRoleunique);
                       document.frmManageuserroles.UserRole.options[j].selected = true;
                       selectRoleaftersubmit(j);
                       return false;
                    }
                }
                
                 
          }
        }
        

        //end
        if (!cansubmit) {
            alert(UserRoleRName)
            return false;
        }
        else
            return true;
	}
	
	function selectRoleaftersubmit(cb)
	{
	  if(cb!= "")
	  {  
	    
		document.frmManageuserroles.RoleName.disabled = false;	
		
		document.getElementById("RNtxt").innerHTML="";
		urary = ((document.frmManageuserroles.userroles.value).split("##"))[cb].split("@@");
		
		document.frmManageuserroles.roleidx.value = cb;
		document.frmManageuserroles.RoleID.value = urary[0];
		document.frmManageuserroles.RoleName.value = urary[1];
		document.frmManageuserroles.MenuMask.value = urary[2];
		document.frmManageuserroles.Active.value = urary[3];
		document.frmManageuserroles.Locked.value = urary[4];
		document.frmManageuserroles.CreateType.value = urary[5]; // FB 1968
		
		ifrmMenumasklist.init();
		
		
		
		document.frmManageuserroles.RoleName.disabled = false;
		document.frmManageuserroles.ManageuserrolesDelete.disabled = false;
		document.frmManageuserroles.ManageuserrolesDelete.className = "altMedium0BlueButtonFormat"; //FB 2664
		document.getElementById("RoleMessage").innerHTML="";
	}
	}
	//ZD 100176 start
	function DataLoading(val) {
	    if (val == "1")
	        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
	    else
	        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
	}
	//ZD 100176 End

//-->
</script>
</head>
<body>
     <%--ZD 102406 Start--%>
     <table style="width:96%;" align="center" border="0">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ManageUserRoles_ManageUserRol%>" runat="server"></asp:Literal>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                   <asp:Label ID="MsgLbl" runat="server" CssClass="lblError" ></asp:Label>
                </td>
            </tr>
            <tr>
                <div id="dataLoadingDIV" style="display:none" align="center" >
                    <img border='0' src='image/wait1.gif'  alt='Loading..' />
                </div>
            </tr>
      </table>   
    <%--ZD 102406 End--%>
    <%--Window Dressing--%>       
   
    <form id="frmManageuserroles" method="post" runat="server">
    <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <input type="hidden" name="cmd" value="SetUserRoles" />
    <input type="hidden" name="userroles" id="userroles" runat="server" />
    <input type="hidden" name="submit" id="submit" runat="server" />
    <input type="hidden" name="roleidx" value="-1" />
    <input type="hidden" name="RoleID" value="" />
    <input type="hidden" name="MenuMaskOLD" value="5*0-6*0+7*0+3*0+6*0+2*0+2*0+5*0+8*0+8*0+2*0+3*0+3*0+3*0+3*0-6*0" />
    <input type="hidden" name="MenuMask" value="8*0-4*0+8*0+4*0+4*0+5*0+4*0+5*0+5*0+2*0+2*0+4*0+8*0+6*0+3*0+9*0+2*0+2*0+2*0+1*0-7*0" /> <%--//FB 1779 && FB 2023 DD2 FB 2593 FB 2885  ZD 101233 ZD 101525 ZD 102123 ZD 102532 ZD 100040--%> 
    <input type="hidden" name="Active" id="Active" value="" />
    <input type="hidden" name="Locked" id="Locked" value="" />
    <input type="hidden" id="helpPage" value="103" />
    <input type="hidden" name="CreateType" id="CreateType" value= "" /> <%--FB 1968--%>
    
    <center>
            
            <table border="0" cellpadding="0" cellspacing="0" style="width:90%" >
                <tr>
                    <td style="width:3%" valign="top">
                      &nbsp;
                    </td>
                    <td>&nbsp;</td>
                    <td  style="width:96%;height:20" align="left">
                    
		                <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserRoles_CreateEditDelet%>" runat="server"></asp:Literal><br />
		                </span>
                        
                        <ul>
                            <li>
                                <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserRoles_ToCreateclick%>" runat="server"></asp:Literal></span>
                            </li>
                            <li>
                                <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserRoles_ToEditselect%>" runat="server"></asp:Literal></span>
		                    </li>
                            <li>
                                <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserRoles_ToDeleteacus%>" runat="server"></asp:Literal><br />
                                </span>
                                <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserRoles_Whenchangesco%>" runat="server"></asp:Literal><br />
                                </span>
                            </li>
                        </ul>
                    </td>
              </tr>
              <tr>
                <td style="height:5"></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td align="center" >
                        <table width="100%" border="0" cellspacing="1" cellpadding="1">
                            <tr> 
                                <td align="left" style="width:25%"> 
                                    <span class="tableblackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserRoles_UserRole%>" runat="server"></asp:Literal></span>
                                </td>
                                <td style="width:5%"></td>
                                <td align="left" style="width:15%">
                                    <span class="tableblackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserRoles_RoleName%>" runat="server"></asp:Literal></span>
                                </td>
                                <td style="width:5%"></td>
                                <td align="left" style="width:50%"> 
                                    <span class="tableblackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageUserRoles_MenuSelection%>" runat="server"></asp:Literal></span>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:ListBox ID="UserRole" runat="server" Height="300"  Width="300px"  CssClass="altLong0SelectFormat" onchange='JavaScript:selectRole(this);'></asp:ListBox>
                                    <table width="250">
				                        <tr>
				                            <td id="RNtxt" runat="server">
				                            </td>
				                        </tr>
				                        <tr>
					                        <td align="left" valign="top" id="RoleMessage"></td>
				                        </tr>
				                        
				                    </table>
                                </td>
                                <td rowspan="4"></td>
                                <td align="left" valign="top" rowspan="4">
                                    <input type="text" id="RoleName" size="20" value="" class="altText" onchange="chksyn(this.value)"  runat="server" onkeyup="syn(this.value);chkLimit(this,'2');" /><br>
                                </td>
                                <td style="width:5%" rowspan="4"></td>
                                <td align="left" valign="top" rowspan="4">
                                    <iframe src="usermenucontroller.aspx?f=frmManageuserroles&n=0&wintype=ifr" id="ifrmMenumasklist" name="ifrmMenumasklist" width="400" height="400"> <%--Edited for FF--%>
                                        <p><asp:Literal Text="<%$ Resources:WebResources, ManageUserRoles_goto%>" runat="server"></asp:Literal> <a href="usermenucontroller.asp?f=frmManageuserroles&n=0&wintype=ifr">Participants</a></p>
                                    </iframe>
                                </td>
                            </tr>
                            <tr>
                              <td align="right">
                                    <input type="button" id="ManageuserrolesDelete"  style="width :100pt" value="<%$ Resources:WebResources, Delete%>" runat="server" class="altMedium0BlueButtonFormat"  onclick="JavaScript: DelUserRole();" /> <%--FB 2664--%>
                              </td>
                            </tr>
                            <tr>
                              <td align="right">
                                <input type="button" id="ManageuserrolesCreate" width="100pt" value="<%$ Resources:WebResources, ConferenceSetup_btnUpdate%>" runat="server" onClick="JavaScript: AddUserRole();" class="altMedium0BlueButtonFormat" /> <%--FB 2664--%>
                              </td>
                            </tr>
                        </table> 
                    </td>
                </tr>
                 <tr>
                    <td style="height:5"  colspan="3" align="center" valign="middle">
                        
                    </td>
                  </tr>
                  
                  <tr>
                    <td style="height:5" colspan="3"></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td align="right">
                        <table width="70%" border="0" cellspacing="0" cellpadding="2">
                          <tr>
	                        <td>
	                            <input type="reset" name="reset" id="btnReset" runat="server" value="<%$ Resources:WebResources, Reset%>" class="altLongBlueButtonFormat" onClick="JavaScript:history.go(0);" Onclientclick="javascript:DataLoading('1');"/><!-- onclick="JavaScript: history.go(0);" --><%--ZD 100176--%>
	                        </td>
	                        <td>
	                            <asp:Button ID="ManageuserrolesSubmit" Text="<%$ Resources:WebResources, ManageUserRoles_ManageuserrolesSubmit%>" CssClass="altLongYellowButtonFormat" runat="server" OnClick="SubmitClick" OnClientClick="javascript:DataLoading('1');" /><%--ZD 100176--%> 
	                        </td>
                          </tr>
                        </table>
                    </td>
                </tr>
            </table>
    </center>
    </form>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<%--ZD 100420 start--%>
<script type="text/javascript">
     document.getElementById('UserRole').setAttribute("onblur", "document.getElementById('RoleName').focus()");
     document.getElementById('RoleName').setAttribute("onfocus", "");
     document.getElementById('RoleName').setAttribute("onblur", "document.getElementById('ifrmMenumasklist').focus()");
     document.getElementById('ifrmMenumasklist').setAttribute("onfocus", "");
     document.getElementById('ifrmMenumasklist').setAttribute("onblur", "document.getElementById('ManageuserrolesDelete').focus()");
     document.getElementById('ManageuserrolesDelete').setAttribute("onfocus", "");
     document.getElementById('ManageuserrolesDelete').setAttribute("onblur", "document.getElementById('ManageuserrolesCreate').focus()");
     document.getElementById('ManageuserrolesCreate').setAttribute("onfocus", "");
     document.getElementById('btnReset').setAttribute("onblur", "document.getElementById('ManageuserrolesSubmit').focus()");
     document.getElementById('ManageuserrolesSubmit').setAttribute("onfocus", "");

	//ZD 101388 Starts
     function CheckMenuSelection() {
         if (document.getElementById("RoleName").value != "") {
             var menuSelection = false;
             var subMenuItem = ["m1", "m2_1", "m2_2", "m2_3", "m2_4", "m3_1", "m3_2", "m4_1", "m4_2", "m4_3", "m4_4", "m5_1_1", "m5_1_2", "m5_1_3", "m5_1_4", "m5_1_5", "m5_2_1", "m5_2_2", "m5_3_1", "m5_3_2", "m5_3_3", "m5_3_4", "m5_3_5", "m5_3_6", "m5_3_7", "m5_3_8", "m5_4", "m5_5", "m5_6_1", "m5_6_2", "m5_7_1", "m5_7_2", "m5_8_1", "m5_8_2", "m6"];

             for (var i = 0; i < subMenuItem.length; i++) {
                 if (window.frames['ifrmMenumasklist'].document.getElementById(subMenuItem[i]).checked == true) {
                     menuSelection = true;
                     break;
                 }
             }

             if (menuSelection == false) {
                 alert("Please select atleast a menu.");
                 return false;
             }
         }
         return true;
     }
	//ZD 101388 End
</script>
 <%--ZD 100420 End--%>

     <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>
