﻿<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>

<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.MCULoadbalanceGroup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<script src="script/CallMonitorJquery/jquery.1.4.2.js" type="text/javascript"></script>
<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="script/managemcuorder.js"></script>
<script type="JavaScript" src="inc/functions.js"></script>
<script type="text/javascript" src="extract.js"></script>

<script type="text/javascript" language="javascript">
    var img = new Image();
    img.src = "../en/image/wait1.gif";

    function getOwnMCUList() {
    
        var mcuInfo = document.getElementById("MCUsInfo").value;

        url = "MCUPopupList.aspx?mcus=" + mcuInfo;

        if (!window.winrtc) {	// has not yet been defined
            winrtc = window.open(url, "", "width=920,height=350,top=0,left=0,resizable=yes,scrollbars=yes,status=no");//ZD 100040
            winrtc.focus();
        } else // has been defined
            if (!winrtc.closed) {     // still open
                winrtc.close();
                winrtc = window.open(url, "", "width=920,height=350,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //ZD 100040
                winrtc.focus();
            } else {
                winrtc = window.open(url, "", "width=920,height=350,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //ZD 100040
                winrtc.focus();
            }
        }

        function DataLoading(val) {
            if (val == "1")
                document.getElementById("dataLoadingDIV").style.display = 'block'; 
            else
                document.getElementById("dataLoadingDIV").style.display = 'none';
        }

        function frmValidator() {
        
            if (!Page_ClientValidate())
                return Page_IsValid;

            var mcuInfo = document.getElementById("MCUsInfo").value;
            if (mcuInfo.trim() == "" || mcuInfo.trim() == "|") {
                alert(AtLeastOneMCU); //ZD 100040
                return false;
            }
            return true;
        }

        function refreshIframe() {
            var iframeId = document.getElementById('ifrmMemberlist');
            iframeId.src = iframeId.src;
        }

        function deleteAllParty() 
        {
            if (document.frmManageMCULBgroup.MCUsInfo.value != "") 
            {
                var isRemove = confirm(RemoveAllMCUs); //ZD 100040
                if (isRemove == false) {
                    return (false);
                }

                document.frmManageMCULBgroup.MCUsInfo.value = "";
                ifrmMemberlist.location.reload();
            }

            return false;
        }
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" id="Head1">
    <title></title>
    <script type="text/javascript" src="inc/functions.js"></script>
</head>
<body>
    <form id="frmManageMCULBgroup" runat="server" onsubmit="DataLoading('1')">
    <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>" />
        </Scripts>
    </asp:ScriptManager>
    <div>
        <input type="hidden" runat="server" id="hdnMCUSelected" />
        <input type="hidden" id="helpPage" value="73" />
        <table width="75%" align="center" cellpadding="4" border="0">
            <tr>
                <td align="center" colspan="4">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text="<%$ Resources:WebResources, MCULBGroup_Create%>"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4" style="width: 1168px">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <div id="dataLoadingDIV" style="display: none" align="center">
                    <img border='0' src='image/wait1.gif' alt='Loading..' />
                </div>
            </tr>
            <tr>
                <td colspan="4" align="left">
                    <span class="reqfldstarText">*</span> <span class="reqfldText">
                        <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ManageGroup2_RequiredField%>"
                            runat="server"></asp:Literal></span>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <span class="subtitleblueblodtext" style="margin-left: -20px">
                        <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, ManageGroup2_GroupInformati%>"
                            runat="server"></asp:Literal></span>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 16%;">
                    <span class="blackblodtext">
                        <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ManageGroup2_GroupName%>"
                            runat="server"></asp:Literal></span> <span class="reqfldstarText">*</span>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="GroupName" runat="server" CssClass="altText" Width="170"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="ReqFieldGName" Text="<%$ Resources:WebResources, Required%>"
                        ErrorMessage="<%$ Resources:WebResources, Required%>" ControlToValidate="GroupName" ValidationGroup="Submit"
                        runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regGName" ControlToValidate="GroupName" Display="dynamic"
                        runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <span class="blackblodtext">
                        <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, ManageGroup2_Description%>"
                            runat="server"></asp:Literal></span>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="GroupDescription" TextMode="MultiLine" Width="200" Rows="4" Columns="15"
                        runat="server" CssClass="altText"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regGrpDescription" ControlToValidate="GroupDescription"
                        Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                        ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                </td>
                <td style="vertical-align: top">
                    <table style="vertical-align: top">
                        <tr>
                            <td align="left">
                                <span class="blackblodtext" style="width: 30px">
                                    <asp:Literal Text="<%$ Resources:WebResources, MCULBGroupCreate_Loadbalance%>" runat="server"></asp:Literal></span>
                            </td>
                            <td align="right">
                                <asp:CheckBox ID="ChkLoadBalance" runat="server" Style="margin-left: 41px"></asp:CheckBox>
                            </td>
                        </tr>
                        <tr style="display:none">
                            <td align="left">
                                <span class="blackblodtext" style="width: 30px">
                                    <asp:Literal ID="Literal19" Text="<%$ Resources:WebResources, MCULBGroupCreate_Pooled%>"
                                        runat="server"></asp:Literal></span>
                            </td>
                            <td align="right">
                                <asp:CheckBox ID="Chkpooled" runat="server" Style="margin-left: 41px"></asp:CheckBox>
                            </td>
                        </tr>
                        <tr style="display:none">
                            <td align="left">
                                <span class="blackblodtext">
                                    <asp:Literal ID="Literal20" Text="<%$ Resources:WebResources, MCULBGroupCreate_LCR%>"
                                        runat="server"></asp:Literal></span>
                            </td>
                            <td align="right">
                                <asp:CheckBox ID="ChkLCR" runat="server" Style="margin-left: 41px"></asp:CheckBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <span class="subtitleblueblodtext" style="margin-left: -20px">
                        <asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, MCULBGroupCreate_MCUAssignment%>"
                            runat="server"></asp:Literal></span>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table cellpadding="2" cellspacing="0" width="100%" height="95">
                        <tr>
                            <td>
                            </td>
                            <td width="90%" bordercolor="#0000FF" align="left">
                                <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                    <tr class="tableHeader">
                                        <td align="center" class="tableHeader" style="width:3%;">
                                            <asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, ConferenceList_btnDelete%>"
                                                runat="server"></asp:Literal>
                                        </td>
                                        <td align="center" class="tableHeader" style="width:20%;">
                                            <asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, approvalstatus_MCU%>"
                                                runat="server"></asp:Literal>
                                        </td>
                                        <td align="center" class="tableHeader" style="width:20%;">
                                            <asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, BridgeDetails_Administrator%>"
                                                runat="server"></asp:Literal>
                                        </td>
                                        <td align="center" class="tableHeader" style="width:27%;">
                                            <asp:Literal ID="Literal21" Text="<%$ Resources:WebResources, ConferenceSetup_TimeZone%>"
                                                runat="server"></asp:Literal>
                                        </td>
                                        <td align="center" class="tableHeader" style="width:10%;;display:none;">
                                            <asp:Literal ID="Literal22" Text="<%$ Resources:WebResources, MCULBGroupCreate_Loadbalance%>"
                                                runat="server"></asp:Literal>
                                        </td>
                                        <td align="center" class="tableHeader" style="width:10%;display:none;">
                                            <asp:Literal ID="Literal23" Text="<%$ Resources:WebResources, MCULBGroupCreate_Overflow%>"
                                                runat="server"></asp:Literal>
                                        </td>
                                        <td align="center" class="tableHeader" style="width:10%;">
                                            <asp:Literal ID="Literal24" Text="<%$ Resources:WebResources, MCULBGroupCreate_LeadMCU%>"
                                                runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="10" align="left" valign="top" width="16%">
                                <span class="blackblodtext">
                                    <asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, ManageGroup2_Members%>"
                                        runat="server"></asp:Literal></span>
                            </td>
                            <td bordercolor="#0000FF" align="center">
                                <table cellpadding="0" cellspacing="0" width="100%" style="height: 99">
                                    <tr>
                                        <td style="width: 100%; height: 100" valign="top" align="left">
                                            <iframe src="MCUMember.aspx?wintype=ifr" name="ifrmMemberlist" id="ifrmMemberlist"
                                                width="100%" height="100" style="vertical-align: top;">                                                
                                                <p>
                                                    <asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, ManageGroup2_goto%>"
                                                        runat="server"></asp:Literal><a href="MCUMember.aspx?wintype=ifr"><asp:Literal
                                                            ID="Literal12" Text="<%$ Resources:WebResources, ManageGroup2_Members%>" runat="server"></asp:Literal></a></p>
                                            </iframe>
                                           
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td style="height: 62" valign="top" align="left">
                </td>
                <td style="width: 30%">
                    <asp:TextBox ID="MCUsStr" runat="server" Width="0px" ForeColor="transparent" BackColor="transparent"
                        BorderStyle="None" BorderColor="Transparent"></asp:TextBox>
                    <asp:TextBox ID="MCUsInfo" runat="server" Width="0px" ForeColor="Black" BackColor="transparent"
                        BorderStyle="None" BorderColor="Transparent"></asp:TextBox>
                </td>
                <td>
                    
                </td>
                <td align="left" style="width: 15%">
                    <table>
                        <tr>
                            <td>
                                <button id="btnRemoveAll" runat="server" class="altMedium0BlueButtonFormat" style="width: 160px"
                                    onclick="javascript:return deleteAllParty();">
                                    <asp:Literal ID="Literal15" Text="<%$ Resources:WebResources, ManageGroup2_RemoveAll%>"
                                        runat="server"></asp:Literal></button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button id="btnAddressBook" runat="server" class="altMedium0BlueButtonFormat" style="width: 160px" 
                                    onclick="getOwnMCUList();">
                                    <asp:Literal ID="Literal16" Text="<%$ Resources:WebResources, MCULBGroupCreate_MCULookup%>"
                                        runat="server"></asp:Literal></button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="left">
                    <span class="subtitleblueblodtext" style="margin-left: -20px">
                        <asp:Literal ID="Literal17" Text="<%$ Resources:WebResources, ManageGroup2_ConfirmYourGr%>"
                            runat="server"></asp:Literal></span>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="right">
                    <button id="btnCancel" runat="server" class="altMedium0BlueButtonFormat" onclick="javascript:DataLoading('1'); window.location.replace('ManageMCUGroups.aspx'); return false;">
                        <asp:Literal ID="Literal18" Text="<%$ Resources:WebResources, ManageGroup2_Cancel%>"
                            runat="server"></asp:Literal></button>
                    <asp:Button ID="Managegroup2Submit" runat="server" Text="<%$ Resources:WebResources, ManageGroup2_Submit%>"
                        ValidationGroup="Submit" Style="width: 150px" OnClick="Managegroup2Submit_Click"
                        OnClientClick="javascript:return frmValidator();" CssClass="altShortBlueButtonFormat" />
                </td>
            </tr>
        </table>
    </div>
    </form>
    <script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>
