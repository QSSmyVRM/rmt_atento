/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

public partial class en_outlookemaillist2 : System.Web.UI.Page
{

    #region Protected Data members

    protected System.Web.UI.WebControls.Label LblError;
    protected System.Web.UI.HtmlControls.HtmlInputHidden xmljs;
    //protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSession;
    //protected System.Web.UI.HtmlControls.HtmlInputHidden alphabet;
    //protected System.Web.UI.HtmlControls.HtmlInputHidden pageNo;
    //protected System.Web.UI.HtmlControls.HtmlInputHidden canAll;
    //protected System.Web.UI.HtmlControls.HtmlInputHidden totalPages;
    //protected System.Web.UI.HtmlControls.HtmlInputHidden xmljs;
    //protected System.Web.UI.HtmlControls.HtmlInputHidden fromSearch;
    //protected System.Web.UI.HtmlControls.HtmlInputHidden FirstName;
    //protected System.Web.UI.HtmlControls.HtmlInputHidden LastName;
    //protected System.Web.UI.HtmlControls.HtmlInputHidden LoginName;

    #endregion       

    #region Private and Public variables

    protected String xmlstr = null;
    protected string errorMessage = "";
    protected string xmlj = "";
    protected int length = 0;    
    protected int i = 0;
    protected string titlealign = "";
    XmlNode node = null;
    XmlNodeList nodes = null;
    protected string[] userID = null;    
    protected string[] strfirstName = null;
    protected string[] strlastName = null;
    protected string[] email = null;

    myVRMNet.NETFunctions obj = null;
    public ns_Logger.Logger log;//FB 1881
   
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("outlookemaillist2.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            log = new ns_Logger.Logger();
            
            if (Session["outLookUpEmail"] != null)
            {
                if (Request.QueryString["srch"] != null)
                {
                    if (Request.QueryString["srch"] == "y" && Request.QueryString["alSel"] == "Y")
                     {
                         xmlstr = Session["outlookEmails"].ToString();
                     }
                }
                 xmlstr = Session["outLookUpEmail"].ToString();
            }
            else
                xmlstr = Session["outlookEmails"].ToString();

            if (xmlstr != null)
                BindData(xmlstr);

                   
        }
        catch (Exception ex)
        {
            LblError.Visible = true;//ZD 100263                        
            log.Trace("Page_Load" + ex.StackTrace);
            LblError.Text = obj.ShowSystemMessage();

        }
        finally
        {
            Session["outLookUpEmail"] = null;
        }


    }
    #endregion 

    #region Bind Data
    private void BindData(String xmlstr)
    {
        try
        {

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlstr);
            if (xmlDoc == null)
            {
                if (Convert.ToString(Session["who_use"]) == "VRM")
                {
                    //errorMessage = "Outcoming XML document is illegal" + "\r\n" + "\r\n";
                    //errorMessage = errorMessage + Convert.ToString(Session["outXML"]);
                    log.Trace("Outcoming XML document is illegal" + "\r\n" + "\r\n" + Convert.ToString(Session["outXML"]));
                    errorMessage  = obj.ShowSystemMessage();//FB 1881
                    Response.Write(errorMessage);
                    //CatchErrors(errorMessage);
                    //Response.Redirect("underconstruction.asp");
                }
                else
                {
                    //Response.Write("<br><br><p align='center'><font size=4><b>Outcoming XML document is illegal<b></font></p>");
                    //Response.Write("<br><br><p align='left'><font size=2><b>" + Convert.ToString(Session["outXML"]) + "<b></font></p>");
                    log.Trace("Outcoming XML document is illegal" + "\r\n" + "\r\n" + Convert.ToString(Session["outXML"]));
                    errorMessage  = obj.ShowSystemMessage();//FB 1881
                    Response.Write(errorMessage);
                }

                xmlDoc = null;
                Response.End();
            }

           
            nodes = xmlDoc.DocumentElement.SelectNodes("user");
            length = nodes.Count;
            userID = new string[length - 1 + 1];
            strfirstName = new string[length - 1 + 1];
            strlastName = new string[length - 1 + 1];
            email = new string[length - 1 + 1];
            foreach (XmlNode nod in nodes)
            {
                strfirstName[i] = nod.SelectSingleNode("userName/userFirstName").InnerText;
                strlastName[i] = nod.SelectSingleNode("userName/userLastName").InnerText;
                email[i] = nod.SelectSingleNode("userEmail").InnerText;
                xmlj = xmlj + strfirstName[i] + "%%" + strlastName[i] + "%%" + email[i] + ";;";
                xmljs.Value = xmlj;
            }

            if (length > 11)
            {
                titlealign = "left";
            }
            else
            {
                titlealign = "center";
            }

            node = null;
            nodes = null;
            xmlDoc = null;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}
