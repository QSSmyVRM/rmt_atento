/* Copyright (C) 2015  myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_Tiers
{
    public partial class Tiers : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtNewTier1Name;
        protected System.Web.UI.WebControls.Table tblNoTier1s;
        protected System.Web.UI.WebControls.DataGrid dgTier1s;
        protected System.Web.UI.HtmlControls.HtmlTableRow trNew; //FB 2670
        //ZD 101244 start
        protected System.Web.UI.WebControls.TextBox txtsecurityemail;
        protected System.Web.UI.WebControls.CheckBox ChkSecure; 
        protected System.Web.UI.HtmlControls.HtmlTableRow trTiers;
        protected System.Web.UI.HtmlControls.HtmlTableRow trTierssubmit;
        //ZD 101244 end
        ns_Logger.Logger log;
        public Tiers()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            //
            // TODO: Add constructor logic here
            //
        }

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ManageTiers.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

                //reqDepartmentName.Enabled = true;
                //                DateTime startTime = DateTime.Now;
                //                Response.Write(("<br>Start time:" + startTime.ToString()));
                lblHeader.Text = obj.GetTranslatedText("Manage Tiers");//FB 1830 - Translation
                errLabel.Text = "";
                txtNewTier1Name.Focus();//FB 2094
                //tblTier2.Visible = false;
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }

                //FB 2670
                if (Session["admin"].ToString().Trim().Equals("3"))
                {//ZD 101244 start
                    trTiers.Visible = false;
                    trTierssubmit.Visible = false;
                }//ZD 101244 end

                if (!IsPostBack)
                    BindData();
            }
            catch (Exception ex)
            {
                //                Response.Write(ex.Message);
                errLabel.Visible = true;
                //errLabel.Text = "PageLoad: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("PageLoad:" + ex.Message);//ZD 100263
            }

        }

        private void BindData()
        {
            try
            {
                String inXML = "";
                inXML += "<GetLocations>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "</GetLocations>";
                log.Trace("getLocations InXML: " + inXML);
                String outXML = obj.CallMyVRMServer("GetLocations", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("getLocations OutXML: " + outXML);
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetLocations/Location");
                    LoadTier1Grid(nodes);
                    Label lblTemp = new Label();
                    DataGridItem dgFooter = (DataGridItem)dgTier1s.Controls[0].Controls[dgTier1s.Controls[0].Controls.Count - 1];
                    lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                    lblTemp.Text = nodes.Count.ToString();
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible=true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "BindData: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindData:" + ex.Message);//ZD 100263
            }
        }

        protected void LoadTier1Grid(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;

                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    Session.Add("Tier1DS", dt);
                }
                else
                {
                    //tblNoTier1s.Visible = true;
                    dv = new DataView();
                    dt = new DataTable();
                    dt.Columns.Add("tier1ID");
                    dt.Columns.Add("tier1Name");
                    DataRow dr = dt.NewRow();
                    dr["tier1ID"] = "";
                    dr["tier1Name"] = obj.GetTranslatedText("No Tiers found.");
                }
                dgTier1s.DataSource = dt;
                dgTier1s.DataBind();

                //ZD 101244 start
                foreach (DataGridItem dgi in dgTier1s.Items)
                {
                    CheckBox ChkSecure = (CheckBox)dgi.FindControl("ChkSecure1");
                      ChkSecure.Enabled = false;
                }
                //ZD 101244 End

                //FB 2670
                if (Session["admin"].ToString() == "3")
                {
                    foreach (DataGridItem dgi in dgTier1s.Items)
                    {
                        LinkButton btnDelete = (LinkButton)dgi.FindControl("btnDelete");
                        
                        //btnDelete.Attributes.Remove("onClick");
                        //btnDelete.Style.Add("cursor", "default");
                        //btnDelete.ForeColor = System.Drawing.Color.Gray;
                        LinkButton btnEdit = (LinkButton)dgi.FindControl("btnEdit");
                        
                        //btnEdit.Attributes.Remove("onClick");
                        //btnEdit.Style.Add("cursor", "default");
                        //btnEdit.ForeColor = System.Drawing.Color.Gray;
                        //ZD 100263
                        btnDelete.Visible = false;
                        btnEdit.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
               // errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("LoadTier1Grid:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        #endregion
        protected void DeleteTier1(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = ""; // <login><userID>11</userID><edit/><delete><departmentID>1</departmentID></delete></login>
                inXML += "<DeleteTier1>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <ID>" + e.Item.Cells[0].Text + "</ID>";
                inXML += "</DeleteTier1>";
                //Response.Write(obj.Transfer(inXML));
                log.Trace("DeleteTier1 Inxml: " + inXML);
                String outXML = obj.CallMyVRMServer("DeleteTier1", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("DeleteTier1 Outxml: " + outXML);
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("ManageTiers.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("DeleteTier1:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void EditTier1(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                //reqDepartmentName.Enabled = false;
                DataTable dtTemp = (DataTable)Session["Tier1DS"];
                dgTier1s.EditItemIndex = e.Item.ItemIndex;
                Session.Add("Tier1ID", e.Item.Cells[0].Text);
                //Response.Write(dtTemp.Rows.Count);
                dgTier1s.DataSource = dtTemp;
                dgTier1s.DataBind();
                foreach (DataGridItem dgi in dgTier1s.Items)
                    if (dgi.ItemIndex.Equals(e.Item.ItemIndex))
                    {
                        LinkButton btnTemp = (LinkButton)dgi.FindControl("btnUpdate");
                        btnTemp.Visible = true;
                        btnTemp = (LinkButton)dgi.FindControl("btnDelete");
                        btnTemp.Visible = false;
                        btnTemp = (LinkButton)dgi.FindControl("btnCancel");
                        btnTemp.Visible = true;
                        btnTemp = (LinkButton)dgi.FindControl("btnEdit");
                        btnTemp.Visible = false;
                        CheckBox ChkSecure = (CheckBox)dgi.FindControl("ChkSecure1");
                        ChkSecure.Enabled = true;
                    }//Response.Write(":in edit");
                Label lblTemp = new Label();
                DataGridItem dgFooter = (DataGridItem)dgTier1s.Controls[0].Controls[dgTier1s.Controls[0].Controls.Count - 1];
                lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                lblTemp.Text = dgTier1s.Items.Count.ToString();
            }
            catch (Exception ex)
            {
              //  errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("EditTier1:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void CancelTier1(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Response.Redirect("ManageTiers.aspx");
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("CancelTier1:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void UpdateTier1(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                TextBox tbTemp = (TextBox)e.Item.FindControl("txtTier1Name");
                //ZD 101244 start
                CheckBox ChkSecure = (CheckBox)e.Item.FindControl("ChkSecure1");
                TextBox txtSecurityemail = (TextBox)e.Item.FindControl("txtsecurityemail1");
                String outXML = UpdateManageTier1(tbTemp, ChkSecure, txtSecurityemail); 
                //ZD 101244 End
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("ManageTiers.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("UpdateTier1:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected String UpdateManageTier1(TextBox tbTemp, CheckBox ChkSecure, TextBox txtsecurityemail) //101244
        {
            try
            {
                String inXML = ""; // <login><userID>11</userID><edit/><delete><departmentID>1</departmentID></delete></login>
                inXML += "<SetTier1>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <ID>" + Session["Tier1ID"].ToString() + "</ID>";
                inXML += "  <Name>" + tbTemp.Text + "</Name>";
                //ZD 101244 start
                if (ChkSecure.Checked)
                    inXML += "<Secure>1</Secure>";
                else
                    inXML += "<Secure>0</Secure>";
                if (!string.IsNullOrEmpty(txtsecurityemail.Text))
                {
                    string[] Securityemail = txtsecurityemail.Text.Split(';');
                    string RoomSecurityemail = "";
                    for (int i = 0; i < Securityemail.Length; i++)
                    {
                        if (!RoomSecurityemail.Contains(Securityemail[i]))
                        {
                            if (i == 0)
                                RoomSecurityemail = RoomSecurityemail + Securityemail[i];
                            else
                                RoomSecurityemail = RoomSecurityemail + ";" + Securityemail[i];
                        }
                    }
                    inXML += "<Securityemail>" + RoomSecurityemail + "</Securityemail>";
                }
                else
                    inXML += "<Securityemail>" + txtsecurityemail.Text + "</Securityemail>";
                
                //ZD 101244 End
                inXML += "</SetTier1>";
                log.Trace("SetTier1 Inxml: " + inXML);
                String outXML = obj.CallMyVRMServer("SetTier1", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("SetTier1 Outxml: " + outXML);
                return outXML;
            }
            catch (Exception ex)
            {
                //return "Error in Setting Tier1";
                log.Trace("UpdateManageTier1 :Error in Setting Tier1" + ex.Message);
                return obj.ShowSystemMessage();//FB 1881
            }
        }

        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this tier ?") + "')"); //FB japnese
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindRowsDeleteMessage:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        protected void CreateNewTier1(Object sender, EventArgs e)
        {
            try
            {
                //reqDepartmentName.Enabled = true;
                Session.Add("Tier1ID", "new");
                String outXML = UpdateManageTier1(txtNewTier1Name,ChkSecure,txtsecurityemail);//ZD 101244
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("ManageTiers.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("CreateNewTier1:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        protected void ManageTier2(Object sender, CommandEventArgs e)
        {
            //Response.Write(e.CommandArgument);
            Session.Add("Tier1ID", e.CommandArgument.ToString().Split('^')[0]);
            Session.Add("Tier1Name", e.CommandArgument.ToString().Split('^')[1]);
            Response.Redirect("ManageTier2.aspx");
        }
    }
}
