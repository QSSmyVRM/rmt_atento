﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Xml;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Google.Apis.Util;
using Google.Apis.Authentication.OAuth2;
using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Requests;
using Google.Apis.Services;
using System.Threading;
using System.Text;


namespace MyVRMNet
{
    class GoogleUtil
    {

        #region  Declaration
    
        #region myVRM Declaration

        ns_Logger.Logger _log;
        private myVRMNet.NETFunctions _obj;

        string _outXML = "";
        StringBuilder _inXML = null;
        XmlDocument _Xdoc = null;

        #endregion

        #region Conference

        int _GoogleSequence = 0;
        int _ISRecur = 0;
        int _ConfId = 0;
        int _InstanceId = 0;

        string _Confname = "";
        string _SConfid = "";
        string _ConfDesc = "";
        string _ConfDate = "";
        string _ConfEnd = "";
        string _RoomQueue="";
        string _RoomName="";
        string _UserEmail="";
        string _UserName="";
        string _ConfHost = "";
        string _ConfEmail = "";
        string _ConfPreEmail = "";
        string _RecurrencePattern = "";
        string _GoogleGUID = "";

        List<string> _Recur = new List<string>();

        #endregion

        #region GoogleCalendar Declaration

        public CalendarService _service;
        public OAuth2Authenticator<WebServerClient> _authenticator;
        private IAuthorizationState _state;
        private Google.Apis.Calendar.v3.Data.Channel _channel;
        Google.Apis.Calendar.v3.Data.Channel _chresp = null;
        Event _EvtInxml = null;
        Event _Createdevent = null;

        private IAuthorizationState AuthState
        {
            get
            {
                return _state ?? HttpContext.Current.Session["AUTH_STATE"] as IAuthorizationState;
            }
        }

        #endregion
       
        #endregion

        #region Constructor
        /// <summary>
        /// GoogleUtil
        /// </summary>
        public GoogleUtil()
        {
            _obj = new myVRMNet.NETFunctions();
            _log = new ns_Logger.Logger();
        }
        #endregion

        #region SericeCall
        /// <summary>
        /// SericeCall
        /// </summary>
        public void SericeCall()
        {
            if (_service == null)
            {
                if (HttpContext.Current.Session["SERVICE_OBJECT"] != null)
                    _service = (CalendarService)HttpContext.Current.Session["SERVICE_OBJECT"];
                else
                {

                    _service = new CalendarService(new BaseClientService.Initializer() { Authenticator = _authenticator = CreateAuthenticator() });
                }
            }

            // Check if we received OAuth2 credentials with this request; if yes: parse it.
            if (HttpContext.Current.Request["code"] != null)
            {
                _authenticator.LoadAccessToken();

            }

            //Events evnts = _service.Events.List(HttpContext.Current.Session["userEmail"].ToString()).Fetch();

        }
        #endregion

        #region CreateAuthenticator
        /// <summary>
        /// CreateAuthenticator
        /// </summary>
        /// <returns></returns>
        private OAuth2Authenticator<WebServerClient> CreateAuthenticator()
        {
            try
            {
                // Register the authenticator.
                var _provider = new WebServerClient(GoogleAuthenticationServer.Description, _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), HttpContext.Current.Session["GoogleClientID"].ToString()), _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), HttpContext.Current.Session["GoogleSecretID"].ToString()));

                var _authenticator =
                    new OAuth2Authenticator<WebServerClient>(_provider, GetAuthorization) { NoCaching = true };
                return _authenticator;
            }
            catch (Exception)
            {

                return null;
            }
        }
        #endregion

        #region GetAuthorization
        /// <summary>
        /// GetAuthorization
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        private IAuthorizationState GetAuthorization(WebServerClient client)
        {
            try
            {
                if (_obj == null)
                    _obj = new myVRMNet.NETFunctions();
                // If this user is already authenticated, then just return the auth state.

                IAuthorizationState state = AuthState;

                if (state != null)
                    return state;

                // Check if an authorization request already is in progress.
                state = client.ProcessUserAuthorization(new HttpRequestInfo(HttpContext.Current.Request));
                if (state == null)
                {
                    string scope = "https://www.googleapis.com/auth/userinfo.email";
                    List<string> scopesl = new List<string>();
                    scopesl.Add(scope);
                    IEnumerable<string> scopes = scopesl.AsEnumerable();
                    OutgoingWebResponse response = client.PrepareRequestUserAuthorization(new[] { scope }, null);
                    if (HttpContext.Current.Session["GoogleRefreshToken"].ToString().IsNullOrEmpty())
                        response.Headers["Location"] += "&access_type=offline&approval_prompt=force";
                    response.Send();

                }

                if (state != null && (!string.IsNullOrEmpty(state.AccessToken) || !string.IsNullOrEmpty(state.RefreshToken)))
                {
                   

                    if (DateTime.Parse(HttpContext.Current.Session["GoogleChanelExpiration"].ToString()) < DateTime.UtcNow)
                    {
                        HttpContext.Current.Session["AUTH_STATE"] = _state = state;

                        _channel = new Google.Apis.Calendar.v3.Data.Channel();
                        _channel.Id = Guid.NewGuid().ToString();

                        _channel.Type = "web_hook";
                        _channel.Address = HttpContext.Current.Session["GoogleChannelAddress"].ToString()+@"\en\GoogleReceiver.aspx";
                        _channel.Token = HttpContext.Current.Session["userID"].ToString();

                        _chresp = new Google.Apis.Calendar.v3.Data.Channel();
                        _chresp = _service.Events.Watch(_channel, HttpContext.Current.Session["userEmail"].ToString()).Execute();

                    }


                    //Check the authenticated 

                    // Store and return the credentials.


                    _inXML = new StringBuilder();
                    _inXML.Append("<UserToken>");
                    _inXML.Append("<UserId>" + HttpContext.Current.Session["userID"].ToString() + "</UserId>");
                    _inXML.Append("<UserEmail>" + HttpContext.Current.Session["userEmail"].ToString() + "</UserEmail>");
                    _inXML.Append(_obj.OrgXMLElement());

                    _inXML.Append("<AccessToken>" + state.AccessToken.ToString() + "</AccessToken>");
                    HttpContext.Current.Session["GoogleAccessToken"] = _obj.GetEncrpytedText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), state.AccessToken.ToString().Trim());

                    if (state.RefreshToken.IsNullOrEmpty())
                        _inXML.Append("<RefreshToken>" + _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), HttpContext.Current.Session["GoogleRefreshToken"].ToString()) + "</RefreshToken>");
                    else
                    {
                        _inXML.Append("<RefreshToken>" + state.RefreshToken.ToString().Trim() + "</RefreshToken>");
                        HttpContext.Current.Session["GoogleRefreshToken"] = _obj.GetEncrpytedText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), state.RefreshToken.ToString().Trim());
                    }

                    if (_chresp != null)
                    {
                        _inXML.Append("<ChannelID>" + _chresp.Id + "</ChannelID>");
                        _inXML.Append("<ChannelEtag>" + _chresp.ETag + "</ChannelEtag>");

                        double _Expiration = 0;
                        double.TryParse(_chresp.Expiration, out _Expiration);

                        DateTime _dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                        _dtDateTime = _dtDateTime.AddMilliseconds(_Expiration).ToUniversalTime();

                        _inXML.Append("<ChannelExpiration>" + _dtDateTime.ToString() + "</ChannelExpiration>");
                        _inXML.Append("<ChannelResourceId>" + _chresp.ResourceId + "</ChannelResourceId>");
                        _inXML.Append("<ChannelResourceUri>" + _chresp.ResourceUri + "</ChannelResourceUri>");
                        HttpContext.Current.Session["GoogleChannelID"] = "";
                        HttpContext.Current.Session["GoogleChannelAddress"] = "";
                        HttpContext.Current.Session["GoogleChanelExpiration"] = "";
                    }

                    _inXML.Append("</UserToken>");

                    _outXML = _obj.CallMyVRMServer("SetUserToken", _inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());


                }
                return null;
            }
            catch (System.Threading.ThreadAbortException ex)
            {
                _log.Trace("" + ex.StackTrace);
                return null;
            }
            catch (Exception e)
            {
                _log.Trace("" + e.StackTrace);
                return null;
            }


        }
        #endregion

        #region ScheduleEvent
        /// <summary>
        /// ScheduleEvent
        /// </summary>
        /// <param name="confroom"></param>
        /// <param name="conf"></param>
        /// <param name="NewConfRooms"></param>
        /// <param name="NewConfUsers"></param>
        /// <returns></returns>
        public string ScheduleEvent(string inxml)
        {
            IList<EventAttendee> _lstOfAtendee = new List<EventAttendee>();
            EventAttendee _Eventattendee = new EventAttendee();
            EventDateTime _DateTime = null;
            XmlNodeList confs = null;
            XmlNode _Node = null;
            
            try
            {
                _EvtInxml = new Event();
                _Xdoc = new XmlDocument();

                if (inxml.IndexOf("<error>") < 0)
                {
                    _Xdoc.LoadXml(inxml);

                    if (_Xdoc.SelectNodes("//conferences/conference") != null)
                        confs = _Xdoc.SelectNodes("//conferences/conference");
                    for (int i = 0; i < confs.Count; i++)
                    {
                        _Node = confs[i];
                        _Confname = "";
                        if (_Node.SelectSingleNode("ExporttoExternal/confName") != null)
                            _Confname = _Node.SelectSingleNode("ExporttoExternal/confName").InnerText;
                        _EvtInxml.Summary = _Confname;

                        _SConfid = "";
                        if (_Node.SelectSingleNode("confID") != null)
                            _SConfid = _Node.SelectSingleNode("confID").InnerText;

                        _ConfDesc = "";
                        if (_Node.SelectSingleNode("ExporttoExternal/confDesc") != null)
                            _ConfDesc = _Node.SelectSingleNode("ExporttoExternal/confDesc").InnerText;
                        _EvtInxml.Description = _ConfDesc;

                        _GoogleSequence = 0;
                        if (_Node.SelectSingleNode("ExporttoExternal/GoogleSequence") != null)
                            int.TryParse(_Node.SelectSingleNode("ExporttoExternal/GoogleSequence").InnerText, out _GoogleSequence);
                        _GoogleSequence = _GoogleSequence + 2; //Sequence should be greater than the last upated sequence value.
                        _EvtInxml.Sequence = _GoogleSequence;

                        _ConfDate = "";
                        _DateTime = new EventDateTime();
                        if (_Node.SelectSingleNode("ExporttoExternal/ConfDate") != null)
                        {
                            _ConfDate = _Node.SelectSingleNode("ExporttoExternal/ConfDate").InnerText;
                            _DateTime.DateTime = DateTime.Parse(_ConfDate).AddSeconds(-45).ToString("yyyy-MM-ddTHH:mm:ss.000Z");
                            _DateTime.TimeZone = "GMT+1:00";
                            _EvtInxml.Start = _DateTime;
                        }

                        _ConfEnd = "";
                        _DateTime = new EventDateTime();
                        if (_Node.SelectSingleNode("ExporttoExternal/ConfEnd") != null)
                        {
                            _ConfEnd = _Node.SelectSingleNode("ExporttoExternal/ConfEnd").InnerText;
                            _DateTime.DateTime = DateTime.Parse(_ConfEnd).ToString("yyyy-MM-ddTHH:mm:ss.000Z");
                            _DateTime.TimeZone = "GMT+1:00";
                            _EvtInxml.End = _DateTime;
                        }


                        XmlNodeList _NodeList = _Node.SelectNodes("ExporttoExternal/conferenceRooms/conferenceRoom");
                        XmlElement _Element = null;
                        foreach (XmlNode _node in _NodeList)
                        {
                            _Element = (XmlElement)_node;

                            _RoomQueue = ""; _RoomName = "";
                            _RoomQueue = _Element.GetElementsByTagName("roomQueue")[0].InnerText;
                            _RoomName = _Element.GetElementsByTagName("roomName")[0].InnerText;

                            _Eventattendee = new EventAttendee();
                            _Eventattendee.Email = _RoomQueue;
                            _Eventattendee.DisplayName = _RoomName;
                            _Eventattendee.Organizer = false;
                            _Eventattendee.Resource = true;
                            _lstOfAtendee.Add(_Eventattendee);
                        }

                        _NodeList = _Node.SelectNodes("ExporttoExternal/conferenceUsers/conferenceUser");
                        _Element = null;
                        foreach (XmlNode _node in _NodeList)
                        {
                            _Element = (XmlElement)_node;
                            _UserEmail = ""; _UserName = "";

                            _UserEmail = _Element.GetElementsByTagName("userEmail")[0].InnerText;
                            _UserName = _Element.GetElementsByTagName("userName")[0].InnerText;

                            _Eventattendee = new EventAttendee();
                            _Eventattendee.Email = _UserEmail;
                            _Eventattendee.DisplayName = _UserName;
                            _Eventattendee.Organizer = false;
                            _Eventattendee.Resource = true;
                            _lstOfAtendee.Add(_Eventattendee);
                        }
                        _EvtInxml.Attendees = _lstOfAtendee;

                        Event.OrganizerData _Organizer = new Event.OrganizerData();
                        _ConfHost = "";
                        if (_Node.SelectNodes("ExporttoExternal/ConfHost/userName") != null)
                            _ConfHost = _Node.SelectSingleNode("ExporttoExternal/ConfHost/userName").InnerText;

                        _ConfEmail = "";
                        if (_Node.SelectSingleNode("ExporttoExternal/ConfHost/userEmail") != null)
                            _ConfEmail = _Node.SelectSingleNode("ExporttoExternal/ConfHost/userEmail").InnerText;

                        _Organizer.Email = _ConfEmail;
                        _Organizer.DisplayName = _ConfHost;
                        _Organizer.Self = true;
                        _EvtInxml.Organizer = _Organizer;

                        _ConfPreEmail = "";
                        if (_Node.SelectSingleNode("ExporttoExternal/ConfLastHost/userEmail") != null)
                            _ConfPreEmail = _Node.SelectSingleNode("ExporttoExternal/ConfLastHost/userEmail").InnerText;

                        _ISRecur = 0;
                        if (_Node.SelectSingleNode("ExporttoExternal/isRecur") != null)
                            int.TryParse(_Node.SelectSingleNode("ExporttoExternal/isRecur").InnerText, out _ISRecur);

                        ExtractConfInstanceIds(_SConfid, ref _ConfId, ref _InstanceId);

                        if (_ISRecur == 1 && _InstanceId == 0) //For Normal conference & whole Recurrence conference
                        {
                            _RecurrencePattern = "";
                            if (_Node.SelectSingleNode("ExporttoExternal/RecurrencePattern") != null)
                                _RecurrencePattern = _Node.SelectSingleNode("ExporttoExternal/RecurrencePattern").InnerText;

                            _Recur.Add(_RecurrencePattern);
                            _EvtInxml.Recurrence = _Recur;
                        }

                        _GoogleGUID = "";
                        if (_Node.SelectSingleNode("ExporttoExternal/GoogleGUID") != null)
                            _GoogleGUID = _Node.SelectSingleNode("ExporttoExternal/GoogleGUID").InnerText;

                        SericeCall();
                        if (string.IsNullOrEmpty(_GoogleGUID)) //For new conference
                        {
                            RefreshTokens();
                            _Createdevent = _service.Events.Insert(_EvtInxml, _ConfEmail.ToString()).Execute();
                        }
                        else  //For update conference
                        {
                            if (_InstanceId > 0 && _ISRecur == 1)
                                _GoogleGUID = _GoogleGUID + "_" + DateTime.Parse(_ConfDate).AddSeconds(-45).ToString("yyyyMMddTHHmmssZ"); //For Instance Edit need to pass Datetime

                            _EvtInxml.Id = _GoogleGUID;
                             
                            RefreshTokens();
                            _Createdevent = new Event();

                            if (!string.IsNullOrEmpty(_ConfPreEmail))
                            {
                                if (!_ConfPreEmail.Equals(_ConfEmail))
                                {
                                    _Createdevent = _service.Events.Move(_ConfPreEmail, _GoogleGUID, _ConfEmail).Execute();
                                }
                            }

                            _Createdevent = _service.Events.Update(_EvtInxml, _ConfEmail.ToString(), _EvtInxml.Id).Execute();
                          
                        }

                        _outXML = "<GoogleData>";
                        _outXML += "<confid>" + _SConfid + "</confid>";

                        if (_InstanceId > 0 && _ISRecur == 1) //For Instance edit Guid exist in RecurringEventId
                            _GoogleGUID = _Createdevent.RecurringEventId;
                        else
                            _GoogleGUID = _Createdevent.Id;

                        _outXML += "<GoogleID>" + _GoogleGUID + "</GoogleID>";
                        _outXML += "<GoogleConfSeq>" + _Createdevent.Sequence + "</GoogleConfSeq>";
                        _outXML += "<GoogleConfUpdated>" + _Createdevent.Updated + "</GoogleConfUpdated>";
                        _outXML += "</GoogleData>";
                    }

                }
                return _outXML;

            }
            catch (Exception ex)
            {
                _log.Trace("ScheduleEvent:" + ex.Message);
                return _outXML;
            }
        }
        #endregion

        #region DeleteEvent
        /// <summary>
        /// DeleteEvent
        /// </summary>
        /// <param name="inxml"></param>
        public void DeleteEvent(string inxml)
        {
           
            try
            {
                _Xdoc = new XmlDocument();
                if (inxml.IndexOf("<error>") < 0)
                {
                    _Xdoc.LoadXml(inxml);

                    _SConfid = "";
                    if (_Xdoc.SelectSingleNode("conferences/conference/confID") != null)
                        _SConfid = _Xdoc.SelectSingleNode("conferences/conference/confID").InnerText;

                    _ConfId = 0; _InstanceId = 0;
                    ExtractConfInstanceIds(_SConfid, ref _ConfId, ref _InstanceId);
                    
                    _ConfDate = "";
                    if (_Xdoc.SelectSingleNode("conferences/conference/ExporttoExternal/ConfDate") != null)
                        _ConfDate = _Xdoc.SelectSingleNode("conferences/conference/ExporttoExternal/ConfDate").InnerText;

                    _GoogleGUID = "";
                    if (_Xdoc.SelectSingleNode("conferences/conference/ExporttoExternal/GoogleGUID") != null)
                        _GoogleGUID = _Xdoc.SelectSingleNode("conferences/conference/ExporttoExternal/GoogleGUID").InnerText;

                    _ConfEmail = "";
                    if (_Xdoc.SelectSingleNode("conferences/conference/ExporttoExternal/HostEmail") != null)
                        _ConfEmail = _Xdoc.SelectSingleNode("conferences/conference/ExporttoExternal/HostEmail").InnerText;

                    _ISRecur = 0;
                    if (_Xdoc.SelectSingleNode("conferences/conference/ExporttoExternal/IsRecur") != null)
                        int.TryParse(_Xdoc.SelectSingleNode("conferences/conference/ExporttoExternal/IsRecur").InnerText, out _ISRecur);

                    if (_InstanceId > 0 && _ISRecur == 1)//For Instance delete need to add DateTime
                        _GoogleGUID = _GoogleGUID + "_" + DateTime.Parse(_ConfDate).AddSeconds(-45).ToString("yyyyMMddTHHmmssZ"); //For Instance Edit need to pass Datetime

                    SericeCall();
                    RefreshTokens();
                    _service.Events.Delete(_ConfEmail, _GoogleGUID).Execute();

                }
            }
            catch (Exception ex)
            {
                _log.Trace("ScheduleEvent:" + ex.Message);
            }
        }
        #endregion

        #region RefreshTokens
        /// <summary>
        /// To refresh Access Token before create,update,deleteRefreshTokens
        /// </summary>
        public void RefreshTokens()
        {
            try
            {
                var _Provider = new NativeApplicationClient(GoogleAuthenticationServer.Description);
                _Provider.ClientIdentifier = _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), HttpContext.Current.Session["GoogleClientID"].ToString());
                _Provider.ClientSecret = _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), HttpContext.Current.Session["GoogleSecretID"].ToString());
                OAuth2Authenticator<NativeApplicationClient> _Auth = new OAuth2Authenticator<NativeApplicationClient>(_Provider, GetState);
                _Auth.LoadAccessToken();
            }
            catch (Exception ex)
            {
                _log.Trace("refreshToken:" + ex.Message);
                throw;
            }
        }
        #endregion

        #region GetState
        /// <summary>
        /// GetState to Get the Access token based on existing refresh token
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public IAuthorizationState GetState(NativeApplicationClient arg)
        {
            try
            {
                IAuthorizationState _State = new AuthorizationState(new[] { CalendarService.Scopes.Calendar.GetStringValue()});
                _State.Callback = new Uri(NativeApplicationClient.OutOfBandCallbackUrl);
                _State.AccessToken = _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), HttpContext.Current.Session["GoogleAccessToken"].ToString());
                _State.RefreshToken = _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), HttpContext.Current.Session["GoogleRefreshToken"].ToString());
                arg.RefreshToken(_State);
                HttpContext.Current.Session["GoogleAccessToken"] = _obj.GetEncrpytedText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), _State.AccessToken);

                return _State;

            }
            catch (Exception ex)
            {
                _log.Trace("getState:" + ex.Message);
                return null;
            }
        }
        #endregion

        #region ExtractConfInstanceIds
        /// <summary>
        /// Common method use to extract the conf id and instance id.
        /// </summary>
        /// <param name="inputStr"></param>
        /// <param name="confId"></param>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        internal bool ExtractConfInstanceIds(string _inputStr, ref int _confId, ref int _instanceId)
        {
            try
            {
                int _delimitter = _inputStr.IndexOf(",");
                if (_delimitter < 0)
                {
                    _confId = Int32.Parse(_inputStr.Trim());
                    _instanceId = 0;
                }
                else
                {
                    _confId = Int32.Parse(_inputStr.Substring(0, _delimitter).Trim());
                    _instanceId = Int32.Parse(_inputStr.Substring(_delimitter + 1).Trim());
                }
                return true;
            }
            catch (Exception e)
            {

                return false;
            }
        }
        #endregion
    }
}
