/*
Zendesk ticket 105652: Issue with Password Reset email.

Issue: Placeholders are appearing in template for password request in Spanish language

Cause: Code to replace placeholders with actual value is absent. 

Resolution: If we add code for user id and password then both will be visible in email body. 
			Therefore, email template should be update to send reset password link.
Created by : Kuldeep Kumar
Create On : 13 July 2017
*/

DECLARE @iEmailContentId INT = 0

--select email template content id for spanish language (request password)
SELECT @iEmailContentId=EmailContentId FROM Email_Content_D WHERE EmailLangId=3 AND emailsubject IN ('Informaci�n de Inicio de Sesi�n VRM Olvidado')
SELECT @iEmailContentId
--take backup of previous email content
SELECT * INTO #tmpPwd FROM Email_Content_D WHERE EmailContentId=@iEmailContentId

--Now update email template content
UPDATE Email_Content_D 
SET EmailBody='<div style="font-family:Arial; font-size:12px;"><p>{1}</p><p><span style="font-family: Arial, Verdana, sans-serif; font-size: 12px; color: #000000">Hola {2},<br /><br />Como solicit�, aqu� est� su informaci�n de inicio de sesi�n myVRM:<br /><br /> Por favor haga clic en el enlace : <strong>{81}</strong><br /><br /><br />Si no ha solicitado esta informaci�n, por favor notif�quelo inmedi�tamente a su administrador myVRM.<br /><br />Es recomendable que cambie de contrase�a cuando inicie la sesi�n en myVRM.<br /><br /><span style="text-decoration: underline;color: #004a97;font-weight: bold">Soporte t�cnico:</span><br /><br/><span style="color: #004a97;font-weight: bold">Contacto : </span>{37}<br /><span style="color: #004a97;font-weight: bold">Correo-e : </span>{38}<br /><span style="color: #004a97;font-weight: bold">Tel�fono : </span>{39}<br /></span></p></div>' 
WHERE EmailContentId=@iEmailContentId

GO
